CREATE TABLE IF NOT EXISTS PESSOA (
  ID INT AUTO_INCREMENT PRIMARY KEY COMMENT 'Id da pessoa',
  NOME VARCHAR(100) NOT NULL COMMENT 'Nome da pessoa',
  DATANASCIMENTO DATE COMMENT 'Data de nascimento da pessoa',
  CPF VARCHAR(14) COMMENT 'CPF da pessoa',
  FUNCIONARIO BOOLEAN COMMENT 'Flag de funcionario da pessoa'
);

CREATE TABLE IF NOT EXISTS PROJETO (
  ID INT AUTO_INCREMENT PRIMARY KEY COMMENT 'Id do projeto',
  NOME VARCHAR(200) NOT NULL COMMENT 'Nome do projeto',
  DATA_INICIO DATE COMMENT 'Data de inicio do projeto',
  DATA_PREVISAO_FIM DATE COMMENT 'Data de previsao do fim do projeto',
  DATA_FIM DATE COMMENT 'Data do fim do projeto',
  DESCRICAO VARCHAR(5000) COMMENT 'Descricao do projeto',
  STATUS VARCHAR(45) COMMENT 'Status do projeto',
  ORCAMENTO FLOAT COMMENT 'Orcamento do projeto',
  RISCO VARCHAR(45) COMMENT 'Status do projeto',
  IDGERENTE INT NOT NULL COMMENT 'Id do gerente do projeto',
  FOREIGN KEY (IDGERENTE) REFERENCES PESSOA(ID)
);

CREATE TABLE IF NOT EXISTS MEMBROS (
  IDPROJETO INT PRIMARY KEY NOT NULL COMMENT 'Id do projeto',
  IDPESSOA INT NOT NULL COMMENT 'Id da pessoa',
  FOREIGN KEY (IDPROJETO) REFERENCES PROJETO(ID),
  FOREIGN KEY (IDPESSOA) REFERENCES PESSOA(ID)
);