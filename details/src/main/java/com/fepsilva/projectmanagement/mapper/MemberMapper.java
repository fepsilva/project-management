package com.fepsilva.projectmanagement.mapper;

import com.fepsilva.projectmanagement.entity.MemberEntity;
import com.fepsilva.projectmanagement.model.Member;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MemberMapper {

	MemberEntity toEntity(Member member);
	Member toModel(MemberEntity memberEntity);

}