package com.fepsilva.projectmanagement.mapper;

import com.fepsilva.projectmanagement.model.Person;
import com.fepsilva.projectmanagement.entity.PersonEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PersonMapper {

	PersonEntity toEntity(Person person);
	Person toModel(PersonEntity personEntity);

}