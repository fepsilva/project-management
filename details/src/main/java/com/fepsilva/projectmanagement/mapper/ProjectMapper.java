package com.fepsilva.projectmanagement.mapper;

import com.fepsilva.projectmanagement.entity.ProjectEntity;
import com.fepsilva.projectmanagement.model.Project;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProjectMapper {

	ProjectEntity toEntity(Project project);
	Project toModel(ProjectEntity projectEntity);

}