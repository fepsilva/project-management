package com.fepsilva.projectmanagement.entity.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusEntityEnum {

	UNDER_ANALYSIS("Em Análise"),
	ANALYSIS_PERFORMED("Análise Realizada"),
	APPROVED_ANALYSIS("Análise Aprovada"),
	STARTED("Iniciado"),
	PLANNED("Planejado"),
	IN_PROGRESS("Em Andamento"),
	CLOSED("Encerrado"),
	CANCELED("Cancelado");

	private String value;

}