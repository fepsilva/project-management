package com.fepsilva.projectmanagement.entity;

import com.fepsilva.projectmanagement.entity.constant.RiskEntityEnum;
import com.fepsilva.projectmanagement.entity.constant.StatusEntityEnum;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PROJETO")
public class ProjectEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "NOME", nullable = false)
	private String name;

	@Column(name = "DATA_INICIO")
	private LocalDate startDate;

	@Column(name = "DATA_PREVISAO_FIM")
	private LocalDate expectedEndDate;

	@Column(name = "DATA_FIM")
	private LocalDate endDate;

	@Column(name = "DESCRICAO")
	private String description;

	@Column(name = "STATUS")
	private StatusEntityEnum status;

	@Column(name = "ORCAMENTO")
	private BigDecimal budget;

	@Column(name = "RISCO")
	private RiskEntityEnum risk;

	@Column(name = "IDGERENTE")
	private Long managerId;

}