package com.fepsilva.projectmanagement.entity.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RiskEntityEnum {

	LOW_RISK("Baixo Risco"),
	MEDIUM_RISK("Médio Risco"),
	HIGH_RISK("Alto Risco");

	private String value;

}