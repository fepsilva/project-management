package com.fepsilva.projectmanagement.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "MEMBRO")
public class MemberEntity {

	@Id
	@Column(name = "IDPROJETO", nullable = false)
	private Long idProject;

	@Column(name = "IDPESSOA", nullable = false)
	private Long idPerson;

}