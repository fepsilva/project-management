package com.fepsilva.projectmanagement.repository;

import com.fepsilva.projectmanagement.dataprovider.h2.ProjectDataProvider;
import com.fepsilva.projectmanagement.entity.ProjectEntity;
import com.fepsilva.projectmanagement.mapper.ProjectMapper;
import com.fepsilva.projectmanagement.model.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class ProjectRepositoryImpl implements ProjectRepository {

	@Autowired
	private ProjectDataProvider projectDataProvider;

	@Autowired
	private ProjectMapper projectMapper;

	@Override
	public Project save(Project project) {
		ProjectEntity projectEntity = projectDataProvider.save(projectMapper.toEntity(project));
		return projectMapper.toModel(projectEntity);
	}

	@Override
	public List<Project> list() {
		return Optional.ofNullable(projectDataProvider.findAll())
				.orElseGet(ArrayList::new)
				.stream().map(projectMapper::toModel)
				.toList();
	}

	@Override
	public Optional<Project> findById(Long id) {
		return Optional.ofNullable(projectDataProvider.findById(id)
				.map(projectMapper::toModel)
				.orElse(null));
	}

	@Override
	public void delete(Project project) {
		projectDataProvider.delete(projectMapper.toEntity(project));
	}

}