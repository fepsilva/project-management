package com.fepsilva.projectmanagement.repository;

import com.fepsilva.projectmanagement.dataprovider.h2.PersonDataProvider;
import com.fepsilva.projectmanagement.entity.PersonEntity;
import com.fepsilva.projectmanagement.mapper.PersonMapper;
import com.fepsilva.projectmanagement.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class PersonRepositoryImpl implements PersonRepository {

	@Autowired
	private PersonDataProvider personDataProvider;

	@Autowired
	private PersonMapper personMapper;

	@Override
	public Person save(Person person) {
		PersonEntity personEntity = personDataProvider.save(personMapper.toEntity(person));
		return personMapper.toModel(personEntity);
	}

	@Override
	public List<Person> list() {
		return Optional.ofNullable(personDataProvider.findAll())
				.orElseGet(ArrayList::new)
				.stream().map(personMapper::toModel)
				.toList();
	}

	@Override
	public Optional<Person> findById(Long id) {
		return Optional.ofNullable(personDataProvider.findById(id)
				.map(personMapper::toModel)
				.orElse(null));
	}

	@Override
	public Optional<Person> findByName(String name) {
		Example<PersonEntity> example = Example.of(PersonEntity.builder()
				.name(name)
				.build());

		return Optional.ofNullable(personDataProvider.findOne(example)
				.map(personMapper::toModel)
				.orElse(null));
	}

	@Override
	public void delete(Person person) {
		personDataProvider.delete(personMapper.toEntity(person));
	}

}