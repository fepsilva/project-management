package com.fepsilva.projectmanagement.repository;

import com.fepsilva.projectmanagement.dataprovider.h2.MemberDataProvider;
import com.fepsilva.projectmanagement.entity.MemberEntity;
import com.fepsilva.projectmanagement.mapper.MemberMapper;
import com.fepsilva.projectmanagement.model.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class MemberRepositoryImpl implements MemberRepository {

	@Autowired
	private MemberDataProvider memberDataProvider;

	@Autowired
	private MemberMapper memberMapper;

	@Override
	public Member save(Member member) {
		MemberEntity memberEntity = memberDataProvider.save(memberMapper.toEntity(member));
		return memberMapper.toModel(memberEntity);
	}

	@Override
	public List<Member> list() {
		return Optional.ofNullable(memberDataProvider.findAll())
				.orElseGet(ArrayList::new)
				.stream().map(memberMapper::toModel)
				.toList();
	}

	@Override
	public Optional<Member> findById(Long id) {
		return Optional.ofNullable(memberDataProvider.findById(id)
				.map(memberMapper::toModel)
				.orElse(null));
	}

	@Override
	public void delete(Member member) {
		memberDataProvider.delete(memberMapper.toEntity(member));
	}

}