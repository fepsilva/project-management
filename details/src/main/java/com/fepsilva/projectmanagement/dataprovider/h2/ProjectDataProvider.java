package com.fepsilva.projectmanagement.dataprovider.h2;

import com.fepsilva.projectmanagement.entity.ProjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectDataProvider extends JpaRepository<ProjectEntity, Long> {

}