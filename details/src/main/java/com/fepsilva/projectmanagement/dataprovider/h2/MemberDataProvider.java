package com.fepsilva.projectmanagement.dataprovider.h2;

import com.fepsilva.projectmanagement.entity.MemberEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberDataProvider extends JpaRepository<MemberEntity, Long> {

}