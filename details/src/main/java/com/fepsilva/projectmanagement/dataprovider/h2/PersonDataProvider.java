package com.fepsilva.projectmanagement.dataprovider.h2;

import com.fepsilva.projectmanagement.entity.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonDataProvider extends JpaRepository<PersonEntity, Long> {

}