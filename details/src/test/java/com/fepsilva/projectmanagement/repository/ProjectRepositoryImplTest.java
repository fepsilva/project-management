package com.fepsilva.projectmanagement.repository;

import com.fepsilva.projectmanagement.constant.RiskModelEnum;
import com.fepsilva.projectmanagement.constant.StatusModelEnum;
import com.fepsilva.projectmanagement.dataprovider.h2.ProjectDataProvider;
import com.fepsilva.projectmanagement.entity.ProjectEntity;
import com.fepsilva.projectmanagement.entity.constant.RiskEntityEnum;
import com.fepsilva.projectmanagement.entity.constant.StatusEntityEnum;
import com.fepsilva.projectmanagement.mapper.ProjectMapper;
import com.fepsilva.projectmanagement.model.Project;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ProjectRepositoryImplTest {

	@Mock
	private ProjectDataProvider projectDataProvider;

	@Mock
	private ProjectMapper projectMapper;

	@InjectMocks
	private ProjectRepositoryImpl projectRepositoryImpl;

	@Test
	@DisplayName("Method save() - Should execute successful")
	public void saveSuccessful() {
		// Given
		Long id = 1L;
		String name = "Teste";

		Project project = buildProject(null, name, null, null, null, null, null, null, null, null);
		Project projectSaved = buildProject(id, name, null, null, null, null, null, null, null, null);

		ProjectEntity projectEntity = buildProjectEntity(null, name, null, null, null, null, null, null, null, null);
		ProjectEntity projectEntitySaved = buildProjectEntity(id, name, null, null, null, null, null, null, null, null);

		// When
		Mockito.when(this.projectMapper.toEntity(project)).thenReturn(projectEntity);
		Mockito.when(this.projectDataProvider.save(projectEntity)).thenReturn(projectEntitySaved);
		Mockito.when(this.projectMapper.toModel(projectEntitySaved)).thenReturn(projectSaved);

		// Then
		Project result = projectRepositoryImpl.save(project);
		Assertions.assertNotNull(result.getId());
	}

	@Test
	@DisplayName("Method list() - Should execute successful")
	public void listSuccessfulWithItems() {
		// Given
		Project project1 = buildProject(1L, null, null, null, null, null, null, null, null, null);
		Project project2 = buildProject(2L, null, null, null, null, null, null, null, null, null);

		ProjectEntity projectEntity1 = buildProjectEntity(1L, null, null, null, null, null, null, null, null, null);
		ProjectEntity projectEntity2 = buildProjectEntity(2L, null, null, null, null, null, null, null, null, null);

		List<ProjectEntity> listProjectEntity = List.of(projectEntity1, projectEntity2);

		// When
		Mockito.when(this.projectDataProvider.findAll()).thenReturn(listProjectEntity);
		Mockito.when(this.projectMapper.toModel(projectEntity1)).thenReturn(project1);
		Mockito.when(this.projectMapper.toModel(projectEntity2)).thenReturn(project2);

		// Then
		List<Project> result = projectRepositoryImpl.list();
		Assertions.assertNotNull(result);
		Assertions.assertEquals(result.size(), 2);
	}

	@Test
	@DisplayName("Method list() - Should return empty")
	public void listWithoutItems() {
		// When
		Mockito.when(this.projectDataProvider.findAll()).thenReturn(null);

		// Then
		List<Project> result = projectRepositoryImpl.list();
		Assertions.assertNotNull(result);
		Assertions.assertEquals(result.size(), 0);
	}

	@Test
	@DisplayName("Method findById() - Should execute successful")
	public void findByIdSuccessful() {
		// Given
		Long id = 1L;

		Project project = buildProject(1L, null, null, null, null, null, null, null, null, null);
		ProjectEntity projectEntity = buildProjectEntity(id, null, null, null, null, null, null, null, null, null);

		// When
		Mockito.when(this.projectDataProvider.findById(id)).thenReturn(Optional.of(projectEntity));
		Mockito.when(this.projectMapper.toModel(projectEntity)).thenReturn(project);

		// Then
		Optional<Project> result = projectRepositoryImpl.findById(id);
		Assertions.assertNotNull(result);
		Assertions.assertTrue(result.isPresent());
	}

	@Test
	@DisplayName("Method findById() - Should return null")
	public void findByIdNotFound() {
		// Given
		Long id = 1L;

		// When
		Mockito.when(this.projectDataProvider.findById(id)).thenReturn(Optional.empty());

		// Then
		Optional<Project> result = projectRepositoryImpl.findById(id);
		Assertions.assertNotNull(result);
		Assertions.assertTrue(result.isEmpty());
	}

	@Test
	@DisplayName("Method delete() - Should execute successful")
	public void deleteSuccessful() {
		// Given
		Project project = buildProject(1L, null, null, null, null, null, null, null, null, null);

		// Then
		Assertions.assertDoesNotThrow(() -> projectRepositoryImpl.delete(project));
	}

	private Project buildProject(Long id, String name, LocalDate startDate, LocalDate expectedEndDate, LocalDate endDate, String description, StatusModelEnum status, BigDecimal budget, RiskModelEnum risk, Long managerId) {
		return Project.builder()
				.id(id)
				.name(name)
				.startDate(startDate)
				.expectedEndDate(expectedEndDate)
				.endDate(endDate)
				.description(description)
				.status(status)
				.budget(budget)
				.risk(risk)
				.managerId(managerId)
				.build();
	}

	private ProjectEntity buildProjectEntity(Long id, String name, LocalDate startDate, LocalDate expectedEndDate, LocalDate endDate, String description, StatusEntityEnum status, BigDecimal budget, RiskEntityEnum risk, Long managerId) {
		return ProjectEntity.builder()
				.id(id)
				.name(name)
				.startDate(startDate)
				.expectedEndDate(expectedEndDate)
				.endDate(endDate)
				.description(description)
				.status(status)
				.budget(budget)
				.risk(risk)
				.managerId(managerId)
				.build();
	}

}