package com.fepsilva.projectmanagement.repository;

import com.fepsilva.projectmanagement.dataprovider.h2.PersonDataProvider;
import com.fepsilva.projectmanagement.entity.PersonEntity;
import com.fepsilva.projectmanagement.mapper.PersonMapper;
import com.fepsilva.projectmanagement.model.Person;
import com.fepsilva.projectmanagement.repository.PersonRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class PersonRepositoryImplTest {

	@Mock
	private PersonDataProvider personDataProvider;

	@Mock
	private PersonMapper personMapper;

	@InjectMocks
	private PersonRepositoryImpl personRepositoryImpl;

	@Test
	@DisplayName("Method save() - Should execute successful")
	public void saveSuccessful() {
		// Given
		Long id = 1L;
		String name = "Teste";

		Person person = buildPerson(null, name, null, null, null);
		Person personSaved = buildPerson(id, name, null, null, null);

		PersonEntity personEntity = buildPersonEntity(null, name, null, null, null);
		PersonEntity personEntitySaved = buildPersonEntity(id, name, null, null, null);

		// When
		Mockito.when(this.personMapper.toEntity(person)).thenReturn(personEntity);
		Mockito.when(this.personDataProvider.save(personEntity)).thenReturn(personEntitySaved);
		Mockito.when(this.personMapper.toModel(personEntitySaved)).thenReturn(personSaved);

		// Then
		Person result = personRepositoryImpl.save(person);
		Assertions.assertNotNull(result.getId());
	}

	@Test
	@DisplayName("Method list() - Should execute successful")
	public void listSuccessfulWithItems() {
		// Given
		Person person1 = buildPerson(1L, null, null, null, null);
		Person person2 = buildPerson(2L, null, null, null, null);

		PersonEntity personEntity1 = buildPersonEntity(1L, null, null, null, null);
		PersonEntity personEntity2 = buildPersonEntity(2L, null, null, null, null);

		List<PersonEntity> listPersonEntity = List.of(personEntity1, personEntity2);

		// When
		Mockito.when(this.personDataProvider.findAll()).thenReturn(listPersonEntity);
		Mockito.when(this.personMapper.toModel(personEntity1)).thenReturn(person1);
		Mockito.when(this.personMapper.toModel(personEntity2)).thenReturn(person2);

		// Then
		List<Person> result = personRepositoryImpl.list();
		Assertions.assertNotNull(result);
		Assertions.assertEquals(result.size(), 2);
	}

	@Test
	@DisplayName("Method list() - Should return empty")
	public void listWithoutItems() {
		// When
		Mockito.when(this.personDataProvider.findAll()).thenReturn(null);

		// Then
		List<Person> result = personRepositoryImpl.list();
		Assertions.assertNotNull(result);
		Assertions.assertEquals(result.size(), 0);
	}

	@Test
	@DisplayName("Method findById() - Should execute successful")
	public void findByIdSuccessful() {
		// Given
		Long id = 1L;

		Person person = buildPerson(1L, null, null, null, null);
		PersonEntity personEntity = buildPersonEntity(id, null, null, null, null);

		// When
		Mockito.when(this.personDataProvider.findById(id)).thenReturn(Optional.of(personEntity));
		Mockito.when(this.personMapper.toModel(personEntity)).thenReturn(person);

		// Then
		Optional<Person> result = personRepositoryImpl.findById(id);
		Assertions.assertNotNull(result);
		Assertions.assertTrue(result.isPresent());
	}

	@Test
	@DisplayName("Method findById() - Should return null")
	public void findByIdNotFound() {
		// Given
		Long id = 1L;

		// When
		Mockito.when(this.personDataProvider.findById(id)).thenReturn(Optional.empty());

		// Then
		Optional<Person> result = personRepositoryImpl.findById(id);
		Assertions.assertNotNull(result);
		Assertions.assertTrue(result.isEmpty());
	}

	@Test
	@DisplayName("Method delete() - Should execute successful")
	public void deleteSuccessful() {
		// Given
		Person person = buildPerson(1L, null, null, null, null);

		// Then
		Assertions.assertDoesNotThrow(() -> personRepositoryImpl.delete(person));
	}

	private Person buildPerson(Long id, String name, LocalDate birthDate, String cpf, Boolean employee) {
		return Person.builder()
				.id(id)
				.name(name)
				.birthDate(birthDate)
				.cpf(cpf)
				.employee(employee)
				.build();
	}

	private PersonEntity buildPersonEntity(Long id, String name, LocalDate birthDate, String cpf, Boolean employee) {
		return PersonEntity.builder()
				.id(id)
				.name(name)
				.birthDate(birthDate)
				.cpf(cpf)
				.employee(employee)
				.build();
	}

}