package com.fepsilva.projectmanagement.repository;

import com.fepsilva.projectmanagement.dataprovider.h2.MemberDataProvider;
import com.fepsilva.projectmanagement.entity.MemberEntity;
import com.fepsilva.projectmanagement.mapper.MemberMapper;
import com.fepsilva.projectmanagement.model.Member;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class MemberRepositoryImplTest {

	@Mock
	private MemberDataProvider memberDataProvider;

	@Mock
	private MemberMapper memberMapper;

	@InjectMocks
	private MemberRepositoryImpl memberRepositoryImpl;

	@Test
	@DisplayName("Method save() - Should execute successful")
	public void saveSuccessful() {
		// Given
		Long idProject = 1L;
		Long idPerson = 1L;

		Member member = buildMember(idProject, idPerson);
		Member memberSaved = buildMember(idProject, idPerson);

		MemberEntity memberEntity = buildMemberEntity(idProject, idPerson);
		MemberEntity memberEntitySaved = buildMemberEntity(idProject, idPerson);

		// When
		Mockito.when(this.memberMapper.toEntity(member)).thenReturn(memberEntity);
		Mockito.when(this.memberDataProvider.save(memberEntity)).thenReturn(memberEntitySaved);
		Mockito.when(this.memberMapper.toModel(memberEntitySaved)).thenReturn(memberSaved);

		// Then
		Member result = memberRepositoryImpl.save(member);
		Assertions.assertNotNull(result.getIdProject());
		Assertions.assertNotNull(result.getIdPerson());
	}

	@Test
	@DisplayName("Method list() - Should execute successful")
	public void listSuccessfulWithItems() {
		// Given
		Member member1 = buildMember(1L, 1L);
		Member member2 = buildMember(2L, 2L);

		MemberEntity memberEntity1 = buildMemberEntity(1L, 1L);
		MemberEntity memberEntity2 = buildMemberEntity(2L, 2L);

		List<MemberEntity> listMemberEntity = List.of(memberEntity1, memberEntity2);

		// When
		Mockito.when(this.memberDataProvider.findAll()).thenReturn(listMemberEntity);
		Mockito.when(this.memberMapper.toModel(memberEntity1)).thenReturn(member1);
		Mockito.when(this.memberMapper.toModel(memberEntity2)).thenReturn(member2);

		// Then
		List<Member> result = memberRepositoryImpl.list();
		Assertions.assertNotNull(result);
		Assertions.assertEquals(result.size(), 2);
	}

	@Test
	@DisplayName("Method list() - Should return empty")
	public void listWithoutItems() {
		// When
		Mockito.when(this.memberDataProvider.findAll()).thenReturn(null);

		// Then
		List<Member> result = memberRepositoryImpl.list();
		Assertions.assertNotNull(result);
		Assertions.assertEquals(result.size(), 0);
	}

	@Test
	@DisplayName("Method findById() - Should execute successful")
	public void findByIdSuccessful() {
		// Given
		Long idProject = 1L;
		Long idPerson = 1L;

		Member member = buildMember(1L, idPerson);
		MemberEntity memberEntity = buildMemberEntity(idProject, idPerson);

		// When
		Mockito.when(this.memberDataProvider.findById(idProject)).thenReturn(Optional.of(memberEntity));
		Mockito.when(this.memberMapper.toModel(memberEntity)).thenReturn(member);

		// Then
		Optional<Member> result = memberRepositoryImpl.findById(idProject);
		Assertions.assertNotNull(result);
		Assertions.assertTrue(result.isPresent());
	}

	@Test
	@DisplayName("Method findById() - Should return null")
	public void findByIdNotFound() {
		// Given
		Long id = 1L;

		// When
		Mockito.when(this.memberDataProvider.findById(id)).thenReturn(Optional.empty());

		// Then
		Optional<Member> result = memberRepositoryImpl.findById(id);
		Assertions.assertNotNull(result);
		Assertions.assertTrue(result.isEmpty());
	}

	@Test
	@DisplayName("Method delete() - Should execute successful")
	public void deleteSuccessful() {
		// Given
		Member member = buildMember(1L, 1L);

		// Then
		Assertions.assertDoesNotThrow(() -> memberRepositoryImpl.delete(member));
	}

	private Member buildMember(Long idProject, Long idPerson) {
		return Member.builder()
				.idProject(idProject)
				.idPerson(idPerson)
				.build();
	}

	private MemberEntity buildMemberEntity(Long idProject, Long idPerson) {
		return MemberEntity.builder()
				.idProject(idProject)
				.idPerson(idPerson)
				.build();
	}

}