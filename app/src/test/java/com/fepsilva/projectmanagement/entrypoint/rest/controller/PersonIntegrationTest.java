package com.fepsilva.projectmanagement.entrypoint.rest.controller;

import br.com.fepsilva.openapi.representation.PersonRequestRepresentation;
import com.fepsilva.projectmanagement.entrypoint.rest.controller.util.IntegrationTestUtil;
import com.jayway.jsonpath.JsonPath;
import org.hamcrest.core.StringContains;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("Integration tests of controller Person, path persons")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@AutoConfigureMockMvc
public class PersonIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	@Transactional
	@DisplayName("Post /persons - Should return 201")
	public void postPersonsShouldReturnCreated() throws Exception {
		// Given
		String name = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = buildPersonRequestRepresentation(name, birthDate, cpf, employee);

		// Then
		addPerson(mockMvc, personRequestRepresentation)
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.id").exists());
	}

	@Test
	@Transactional
	@DisplayName("Post /persons - Without name should return 400")
	public void postPersonsWithoutNameShouldReturnBadRequest() throws Exception {
		// Given
		String name = null;
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = buildPersonRequestRepresentation(name, birthDate, cpf, employee);

		// Then
		addPerson(mockMvc, personRequestRepresentation)
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.message", StringContains.containsString("name")));
	}

	@Test
	@Transactional
	@DisplayName("Get /persons - Should return 200")
	public void getPersonsShouldReturnOk() throws Exception {
		// Given
		String name = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation1 = buildPersonRequestRepresentation(name, birthDate, cpf, employee);
		PersonRequestRepresentation personRequestRepresentation2 = buildPersonRequestRepresentation(name, birthDate, cpf, employee);
		PersonRequestRepresentation personRequestRepresentation3 = buildPersonRequestRepresentation(name, birthDate, cpf, employee);

		// When
		addPerson(mockMvc, personRequestRepresentation1);
		addPerson(mockMvc, personRequestRepresentation2);
		addPerson(mockMvc, personRequestRepresentation3);

		// Then
		findAllPerson(mockMvc)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").exists())
				.andExpect(jsonPath("$").isArray())
				.andExpect(jsonPath("$", hasSize(3)));
	}

	@Test
	@Transactional
	@DisplayName("Get /persons/{id} - Should return 200")
	public void getPersonsIdShouldReturnOk() throws Exception {
		// Given
		String name = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = buildPersonRequestRepresentation(name, birthDate, cpf, employee);

		// When
		Long id = Long.parseLong(JsonPath.read(addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		// Then
		findByIdPerson(mockMvc, id)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(id));
	}

	@Test
	@Transactional
	@DisplayName("Get /persons/{id} - Should return 404 on non-existent identifier")
	public void getPersonsIdShouldReturnNotFound() throws Exception {
		// Given
		Long id = 99999999L;

		// Then
		findByIdPerson(mockMvc, id)
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	@DisplayName("Put /persons/{id} - Should return 200")
	public void putPersonsIdShouldReturnOk() throws Exception {
		// Given
		String name = "Teste";
		String nameChanged = "Teste Alterado";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = buildPersonRequestRepresentation(name, birthDate, cpf, employee);

		// When
		Long id = Long.parseLong(JsonPath.read(addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		// Then
		personRequestRepresentation = buildPersonRequestRepresentation(nameChanged, birthDate, cpf, employee);

		putPerson(mockMvc, id, personRequestRepresentation)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(id))
				.andExpect(jsonPath("$.name").value(nameChanged));
	}

	@Test
	@Transactional
	@DisplayName("Put /persons/{id} - Should return 404 on non-existent identifier")
	public void putPersonsIdShouldReturnNotFound() throws Exception {
		// Given
		Long id = 99999999L;
		String name = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = buildPersonRequestRepresentation(name, birthDate, cpf, employee);

		// Then
		putPerson(mockMvc, id, personRequestRepresentation)
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	@DisplayName("Delete /persons/{id} - Should return 200")
	public void deletePersonsIdShouldReturnOk() throws Exception {
		// Given
		String name = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = buildPersonRequestRepresentation(name, birthDate, cpf, employee);

		// When
		Long id = Long.parseLong(JsonPath.read(addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		// Then
		deletePerson(mockMvc, id)
				.andExpect(status().isOk());
	}

	@Test
	@Transactional
	@DisplayName("Delete /persons/{id} - Should return 404 on non-existent identifier")
	public void deletePersonsIdShouldReturnNotFound() throws Exception {
		// Given
		Long id = 99999999L;

		// Then
		deletePerson(mockMvc, id)
				.andExpect(status().isNotFound());
	}

	// Métodos auxiliares
	public static ResultActions addPerson(MockMvc mockMvc, PersonRequestRepresentation personRequestRepresentation) throws Exception {
		return mockMvc.perform(MockMvcRequestBuilders
				.post("/persons")
				.contentType(MediaType.APPLICATION_JSON)
				.content(IntegrationTestUtil.convertObjectToJson(personRequestRepresentation)));
	}

	public static ResultActions findAllPerson(MockMvc mockMvc) throws Exception {
		return mockMvc.perform(MockMvcRequestBuilders
				.get("/persons")
				.contentType(MediaType.APPLICATION_JSON));
	}

	public static ResultActions findByIdPerson(MockMvc mockMvc, Long id) throws Exception {
		return mockMvc.perform(MockMvcRequestBuilders
				.get("/persons/" + id)
				.contentType(MediaType.APPLICATION_JSON));
	}

	public static ResultActions putPerson(MockMvc mockMvc, Long id, PersonRequestRepresentation personRequestRepresentation) throws Exception {
		return mockMvc.perform(MockMvcRequestBuilders
				.put("/persons/" + id)
				.contentType(MediaType.APPLICATION_JSON)
				.content(IntegrationTestUtil.convertObjectToJson(personRequestRepresentation)));
	}

	public static ResultActions deletePerson(MockMvc mockMvc, Long id) throws Exception {
		return mockMvc.perform(MockMvcRequestBuilders
				.delete("/persons/" + id)
				.contentType(MediaType.APPLICATION_JSON));
	}

	public static PersonRequestRepresentation buildPersonRequestRepresentation(String name, String birthDate, String cpf, Boolean employee) {
		PersonRequestRepresentation personRequestRepresentation = new PersonRequestRepresentation();
		personRequestRepresentation.setName(name);
		personRequestRepresentation.setBirthDate(birthDate);
		personRequestRepresentation.setCpf(cpf);
		personRequestRepresentation.setEmployee(employee);

		return personRequestRepresentation;
	}

}