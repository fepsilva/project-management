package com.fepsilva.projectmanagement.entrypoint.rest.controller;

import br.com.fepsilva.openapi.representation.MemberRequestRepresentation;
import br.com.fepsilva.openapi.representation.PersonRequestRepresentation;
import br.com.fepsilva.openapi.representation.ProjectRequestRepresentation;
import com.fepsilva.projectmanagement.entrypoint.rest.controller.util.IntegrationTestUtil;
import com.jayway.jsonpath.JsonPath;
import org.hamcrest.core.StringContains;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("Integration tests of controller Member, path members")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@AutoConfigureMockMvc
public class MemberIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	@Transactional
	@DisplayName("Post /members - Should return 201")
	public void postMembersShouldReturnCreated() throws Exception {
		// Given
		String namePerson = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = PersonIntegrationTest.buildPersonRequestRepresentation(namePerson, birthDate, cpf, employee);
		Long idPerson = Long.parseLong(JsonPath.read(PersonIntegrationTest.addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String nameProject = "Teste";
		String startDate = "2000-12-31";
		String expectedEndDate = "2000-12-31";
		String endDate = "2000-12-31";
		String description = "Teste";
		ProjectRequestRepresentation.StatusEnum status = ProjectRequestRepresentation.StatusEnum.INICIADO;
		BigDecimal budget = BigDecimal.ONE;
		ProjectRequestRepresentation.RiskEnum risk = ProjectRequestRepresentation.RiskEnum.BAIXO_RISCO;

		ProjectRequestRepresentation projectRequestRepresentation = ProjectIntegrationTest.buildProjectRequestRepresentation(nameProject, startDate, expectedEndDate, endDate, description, status, budget, risk, idPerson);
		Long idProject = Long.parseLong(JsonPath.read(ProjectIntegrationTest.addProject(mockMvc, projectRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String rolePerson = "Programador";
		MemberRequestRepresentation memberRequestRepresentation = buildMemberRequestRepresentation(idProject, namePerson, rolePerson);

		// Then
		addMember(mockMvc, memberRequestRepresentation)
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.idProject").exists())
				.andExpect(jsonPath("$.idPerson").exists());
	}

	@Test
	@Transactional
	@DisplayName("Get /members - Should return 200")
	public void getMembersShouldReturnOk() throws Exception {
		// Given
		String namePerson = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = PersonIntegrationTest.buildPersonRequestRepresentation(namePerson, birthDate, cpf, employee);
		Long idPerson = Long.parseLong(JsonPath.read(PersonIntegrationTest.addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String nameProject = "Teste";
		String startDate = "2000-12-31";
		String expectedEndDate = "2000-12-31";
		String endDate = "2000-12-31";
		String description = "Teste";
		ProjectRequestRepresentation.StatusEnum status = ProjectRequestRepresentation.StatusEnum.INICIADO;
		BigDecimal budget = BigDecimal.ONE;
		ProjectRequestRepresentation.RiskEnum risk = ProjectRequestRepresentation.RiskEnum.BAIXO_RISCO;

		ProjectRequestRepresentation projectRequestRepresentation = ProjectIntegrationTest.buildProjectRequestRepresentation(nameProject, startDate, expectedEndDate, endDate, description, status, budget, risk, idPerson);
		Long idProject1 = Long.parseLong(JsonPath.read(ProjectIntegrationTest.addProject(mockMvc, projectRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		Long idProject2 = Long.parseLong(JsonPath.read(ProjectIntegrationTest.addProject(mockMvc, projectRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		Long idProject3 = Long.parseLong(JsonPath.read(ProjectIntegrationTest.addProject(mockMvc, projectRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String rolePerson = "Programador";

		// When
		MemberRequestRepresentation memberRequestRepresentation = buildMemberRequestRepresentation(idProject1, namePerson, rolePerson);
		addMember(mockMvc, memberRequestRepresentation);

		memberRequestRepresentation = buildMemberRequestRepresentation(idProject2, namePerson, rolePerson);
		addMember(mockMvc, memberRequestRepresentation);

		memberRequestRepresentation = buildMemberRequestRepresentation(idProject3, namePerson, rolePerson);
		addMember(mockMvc, memberRequestRepresentation);

		// Then
		findAllMember(mockMvc)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").exists())
				.andExpect(jsonPath("$").isArray())
				.andExpect(jsonPath("$", hasSize(3)));
	}

	@Test
	@Transactional
	@DisplayName("Get /members/{id} - Should return 200")
	public void getMembersIdShouldReturnOk() throws Exception {
		// Given
		String namePerson = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = PersonIntegrationTest.buildPersonRequestRepresentation(namePerson, birthDate, cpf, employee);
		Long idPerson = Long.parseLong(JsonPath.read(PersonIntegrationTest.addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String nameProject = "Teste";
		String startDate = "2000-12-31";
		String expectedEndDate = "2000-12-31";
		String endDate = "2000-12-31";
		String description = "Teste";
		ProjectRequestRepresentation.StatusEnum status = ProjectRequestRepresentation.StatusEnum.INICIADO;
		BigDecimal budget = BigDecimal.ONE;
		ProjectRequestRepresentation.RiskEnum risk = ProjectRequestRepresentation.RiskEnum.BAIXO_RISCO;

		ProjectRequestRepresentation projectRequestRepresentation = ProjectIntegrationTest.buildProjectRequestRepresentation(nameProject, startDate, expectedEndDate, endDate, description, status, budget, risk, idPerson);
		Long idProject = Long.parseLong(JsonPath.read(ProjectIntegrationTest.addProject(mockMvc, projectRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String rolePerson = "Programador";
		MemberRequestRepresentation memberRequestRepresentation = buildMemberRequestRepresentation(idProject, namePerson, rolePerson);

		// When
		addMember(mockMvc, memberRequestRepresentation);

		// Then
		findByIdMember(mockMvc, idProject)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.idProject").value(idProject));
	}

	@Test
	@Transactional
	@DisplayName("Get /members/{id} - Should return 404 on non-existent identifier")
	public void getMembersIdShouldReturnNotFound() throws Exception {
		// Given
		Long idProject = 99999999L;

		// Then
		findByIdMember(mockMvc, idProject)
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	@DisplayName("Delete /members/{id} - Should return 200")
	public void deleteMembersIdShouldReturnOk() throws Exception {
		// Given
		String namePerson = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = PersonIntegrationTest.buildPersonRequestRepresentation(namePerson, birthDate, cpf, employee);
		Long idPerson = Long.parseLong(JsonPath.read(PersonIntegrationTest.addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String nameProject = "Teste";
		String startDate = "2000-12-31";
		String expectedEndDate = "2000-12-31";
		String endDate = "2000-12-31";
		String description = "Teste";
		ProjectRequestRepresentation.StatusEnum status = ProjectRequestRepresentation.StatusEnum.INICIADO;
		BigDecimal budget = BigDecimal.ONE;
		ProjectRequestRepresentation.RiskEnum risk = ProjectRequestRepresentation.RiskEnum.BAIXO_RISCO;

		ProjectRequestRepresentation projectRequestRepresentation = ProjectIntegrationTest.buildProjectRequestRepresentation(nameProject, startDate, expectedEndDate, endDate, description, status, budget, risk, idPerson);
		Long idProject = Long.parseLong(JsonPath.read(ProjectIntegrationTest.addProject(mockMvc, projectRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String rolePerson = "Programador";
		MemberRequestRepresentation memberRequestRepresentation = buildMemberRequestRepresentation(idProject, namePerson, rolePerson);

		// When
		addMember(mockMvc, memberRequestRepresentation);

		// Then
		deleteMember(mockMvc, idProject)
				.andExpect(status().isOk());
	}

	@Test
	@Transactional
	@DisplayName("Delete /members/{id} - Should return 404 on non-existent identifier")
	public void deleteMembersIdShouldReturnNotFound() throws Exception {
		// Given
		Long idProject = 99999999L;

		// Then
		deleteMember(mockMvc, idProject)
				.andExpect(status().isNotFound());
	}

	// Métodos auxiliares
	public static ResultActions addMember(MockMvc mockMvc, MemberRequestRepresentation memberRequestRepresentation) throws Exception {
		return mockMvc.perform(MockMvcRequestBuilders
				.post("/members")
				.contentType(MediaType.APPLICATION_JSON)
				.content(IntegrationTestUtil.convertObjectToJson(memberRequestRepresentation)));
	}

	public static ResultActions findAllMember(MockMvc mockMvc) throws Exception {
		return mockMvc.perform(MockMvcRequestBuilders
				.get("/members")
				.contentType(MediaType.APPLICATION_JSON));
	}

	public static ResultActions findByIdMember(MockMvc mockMvc, Long idProject) throws Exception {
		return mockMvc.perform(MockMvcRequestBuilders
				.get("/members/" + idProject)
				.contentType(MediaType.APPLICATION_JSON));
	}

	public static ResultActions deleteMember(MockMvc mockMvc, Long idProject) throws Exception {
		return mockMvc.perform(MockMvcRequestBuilders
				.delete("/members/" + idProject)
				.contentType(MediaType.APPLICATION_JSON));
	}

	public MemberRequestRepresentation buildMemberRequestRepresentation(Long idProject, String namePerson, String rolePerson) {
		MemberRequestRepresentation memberRequestRepresentation = new MemberRequestRepresentation();
		memberRequestRepresentation.setIdProject(idProject);
		memberRequestRepresentation.setNamePerson(namePerson);
		memberRequestRepresentation.setRolePerson(rolePerson);

		return memberRequestRepresentation;
	}

}