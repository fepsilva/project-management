package com.fepsilva.projectmanagement.entrypoint.rest.controller;

import br.com.fepsilva.openapi.representation.PersonRequestRepresentation;
import br.com.fepsilva.openapi.representation.ProjectRequestRepresentation;
import com.fepsilva.projectmanagement.entrypoint.rest.controller.util.IntegrationTestUtil;
import com.jayway.jsonpath.JsonPath;
import org.hamcrest.core.StringContains;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("Integration tests of controller Project, path projects")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@AutoConfigureMockMvc
public class ProjectIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	@Transactional
	@DisplayName("Post /projects - Should return 201")
	public void postPersonsShouldReturnCreated() throws Exception {
		// Given
		String namePerson = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = PersonIntegrationTest.buildPersonRequestRepresentation(namePerson, birthDate, cpf, employee);
		Long idPerson = Long.parseLong(JsonPath.read(PersonIntegrationTest.addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String name = "Teste";
		String startDate = "2000-12-31";
		String expectedEndDate = "2000-12-31";
		String endDate = "2000-12-31";
		String description = "Teste";
		ProjectRequestRepresentation.StatusEnum status = ProjectRequestRepresentation.StatusEnum.INICIADO;
		BigDecimal budget = BigDecimal.ONE;
		ProjectRequestRepresentation.RiskEnum risk = ProjectRequestRepresentation.RiskEnum.BAIXO_RISCO;

		ProjectRequestRepresentation projectRequestRepresentation = buildProjectRequestRepresentation(name, startDate, expectedEndDate, endDate, description, status, budget, risk, idPerson);

		// Then
		addProject(mockMvc, projectRequestRepresentation)
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.id").exists());
	}

	@Test
	@Transactional
	@DisplayName("Post /projects - Without name should return 400")
	public void postProjectsWithoutNameShouldReturnBadRequest() throws Exception {
		// Given
		String namePerson = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = PersonIntegrationTest.buildPersonRequestRepresentation(namePerson, birthDate, cpf, employee);
		Long idPerson = Long.parseLong(JsonPath.read(PersonIntegrationTest.addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String name = null;
		String startDate = "2000-12-31";
		String expectedEndDate = "2000-12-31";
		String endDate = "2000-12-31";
		String description = "Teste";
		ProjectRequestRepresentation.StatusEnum status = ProjectRequestRepresentation.StatusEnum.INICIADO;
		BigDecimal budget = BigDecimal.ONE;
		ProjectRequestRepresentation.RiskEnum risk = ProjectRequestRepresentation.RiskEnum.BAIXO_RISCO;

		ProjectRequestRepresentation projectRequestRepresentation = buildProjectRequestRepresentation(name, startDate, expectedEndDate, endDate, description, status, budget, risk, idPerson);

		// Then
		addProject(mockMvc, projectRequestRepresentation)
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.message", StringContains.containsString("name")));
	}

	@Test
	@Transactional
	@DisplayName("Get /projects - Should return 200")
	public void getProjectsShouldReturnOk() throws Exception {
		// Given
		String namePerson = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = PersonIntegrationTest.buildPersonRequestRepresentation(namePerson, birthDate, cpf, employee);
		Long idPerson = Long.parseLong(JsonPath.read(PersonIntegrationTest.addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String name = "Teste";
		String startDate = "2000-12-31";
		String expectedEndDate = "2000-12-31";
		String endDate = "2000-12-31";
		String description = "Teste";
		ProjectRequestRepresentation.StatusEnum status = ProjectRequestRepresentation.StatusEnum.INICIADO;
		BigDecimal budget = BigDecimal.ONE;
		ProjectRequestRepresentation.RiskEnum risk = ProjectRequestRepresentation.RiskEnum.BAIXO_RISCO;

		ProjectRequestRepresentation projectRequestRepresentation1 = buildProjectRequestRepresentation(name, startDate, expectedEndDate, endDate, description, status, budget, risk, idPerson);
		ProjectRequestRepresentation projectRequestRepresentation2 = buildProjectRequestRepresentation(name, startDate, expectedEndDate, endDate, description, status, budget, risk, idPerson);
		ProjectRequestRepresentation projectRequestRepresentation3 = buildProjectRequestRepresentation(name, startDate, expectedEndDate, endDate, description, status, budget, risk, idPerson);

		// When
		addProject(mockMvc, projectRequestRepresentation1);
		addProject(mockMvc, projectRequestRepresentation2);
		addProject(mockMvc, projectRequestRepresentation3);

		// Then
		findAllProject(mockMvc)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").exists())
				.andExpect(jsonPath("$").isArray())
				.andExpect(jsonPath("$", hasSize(3)));
	}

	@Test
	@Transactional
	@DisplayName("Get /projects/{id} - Should return 200")
	public void getProjectsIdShouldReturnOk() throws Exception {
		// Given
		String namePerson = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = PersonIntegrationTest.buildPersonRequestRepresentation(namePerson, birthDate, cpf, employee);
		Long idPerson = Long.parseLong(JsonPath.read(PersonIntegrationTest.addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String name = "Teste";
		String startDate = "2000-12-31";
		String expectedEndDate = "2000-12-31";
		String endDate = "2000-12-31";
		String description = "Teste";
		ProjectRequestRepresentation.StatusEnum status = ProjectRequestRepresentation.StatusEnum.INICIADO;
		BigDecimal budget = BigDecimal.ONE;
		ProjectRequestRepresentation.RiskEnum risk = ProjectRequestRepresentation.RiskEnum.BAIXO_RISCO;

		ProjectRequestRepresentation projectRequestRepresentation = buildProjectRequestRepresentation(name, startDate, expectedEndDate, endDate, description, status, budget, risk, idPerson);

		// When
		Long id = Long.parseLong(JsonPath.read(addProject(mockMvc, projectRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		// Then
		findByIdProject(mockMvc, id)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(id));
	}

	@Test
	@Transactional
	@DisplayName("Get /projects/{id} - Should return 404 on non-existent identifier")
	public void getProjectsIdShouldReturnNotFound() throws Exception {
		// Given
		Long id = 99999999L;

		// Then
		findByIdProject(mockMvc, id)
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	@DisplayName("Put /projects/{id} - Should return 200")
	public void putProjectsIdShouldReturnOk() throws Exception {
		// Given
		String namePerson = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = PersonIntegrationTest.buildPersonRequestRepresentation(namePerson, birthDate, cpf, employee);
		Long idPerson = Long.parseLong(JsonPath.read(PersonIntegrationTest.addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String name = "Teste";
		String nameChanged = "Teste Alterado";
		String startDate = "2000-12-31";
		String expectedEndDate = "2000-12-31";
		String endDate = "2000-12-31";
		String description = "Teste";
		ProjectRequestRepresentation.StatusEnum status = ProjectRequestRepresentation.StatusEnum.INICIADO;
		BigDecimal budget = BigDecimal.ONE;
		ProjectRequestRepresentation.RiskEnum risk = ProjectRequestRepresentation.RiskEnum.BAIXO_RISCO;

		ProjectRequestRepresentation projectRequestRepresentation = buildProjectRequestRepresentation(name, startDate, expectedEndDate, endDate, description, status, budget, risk, idPerson);

		// When
		Long id = Long.parseLong(JsonPath.read(addProject(mockMvc, projectRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		// Then
		projectRequestRepresentation = buildProjectRequestRepresentation(nameChanged, startDate, expectedEndDate, endDate, description, status, budget, risk, idPerson);

		putProject(mockMvc, id, projectRequestRepresentation)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(id))
				.andExpect(jsonPath("$.name").value(nameChanged));
	}

	@Test
	@Transactional
	@DisplayName("Put /projects/{id} - Should return 404 on non-existent identifier")
	public void putProjectsIdShouldReturnNotFound() throws Exception {
		// Given
		String namePerson = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = PersonIntegrationTest.buildPersonRequestRepresentation(namePerson, birthDate, cpf, employee);
		Long idPerson = Long.parseLong(JsonPath.read(PersonIntegrationTest.addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		Long id = 99999999L;
		String name = "Teste";
		String startDate = "2000-12-31";
		String expectedEndDate = "2000-12-31";
		String endDate = "2000-12-31";
		String description = "Teste";
		ProjectRequestRepresentation.StatusEnum status = ProjectRequestRepresentation.StatusEnum.INICIADO;
		BigDecimal budget = BigDecimal.ONE;
		ProjectRequestRepresentation.RiskEnum risk = ProjectRequestRepresentation.RiskEnum.BAIXO_RISCO;

		ProjectRequestRepresentation projectRequestRepresentation = buildProjectRequestRepresentation(name, startDate, expectedEndDate, endDate, description, status, budget, risk, idPerson);

		// Then
		putProject(mockMvc, id, projectRequestRepresentation)
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	@DisplayName("Patch /projects/{id} - Should return 200 without previous status")
	public void patchProjectsIdWithoutPreviousStatusShouldReturnOk() throws Exception {
		// Given
		String namePerson = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = PersonIntegrationTest.buildPersonRequestRepresentation(namePerson, birthDate, cpf, employee);
		Long idPerson = Long.parseLong(JsonPath.read(PersonIntegrationTest.addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String name = "Teste";
		String startDate = "2000-12-31";
		String expectedEndDate = "2000-12-31";
		String endDate = "2000-12-31";
		String description = "Teste";
		BigDecimal budget = BigDecimal.ONE;
		ProjectRequestRepresentation.RiskEnum risk = ProjectRequestRepresentation.RiskEnum.BAIXO_RISCO;
		Long managerId = 1L;

		ProjectRequestRepresentation projectRequestRepresentation = buildProjectRequestRepresentation(name, startDate, expectedEndDate, endDate, description, null, budget, risk, idPerson);

		// When
		Long id = Long.parseLong(JsonPath.read(addProject(mockMvc, projectRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		// Then
		patchProject(mockMvc, id)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(id))
				.andExpect(jsonPath("$.status").value(ProjectRequestRepresentation.StatusEnum.EM_AN_LISE.getValue()));
	}

	@Test
	@Transactional
	@DisplayName("Patch /projects/{id} - Should return 200 with previous status")
	public void patchProjectsIdWithPreviousStatusShouldReturnOk() throws Exception {
		// Given
		String namePerson = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = PersonIntegrationTest.buildPersonRequestRepresentation(namePerson, birthDate, cpf, employee);
		Long idPerson = Long.parseLong(JsonPath.read(PersonIntegrationTest.addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String name = "Teste";
		String startDate = "2000-12-31";
		String expectedEndDate = "2000-12-31";
		String endDate = "2000-12-31";
		String description = "Teste";
		ProjectRequestRepresentation.StatusEnum status = ProjectRequestRepresentation.StatusEnum.EM_AN_LISE;
		BigDecimal budget = BigDecimal.ONE;
		ProjectRequestRepresentation.RiskEnum risk = ProjectRequestRepresentation.RiskEnum.BAIXO_RISCO;

		ProjectRequestRepresentation projectRequestRepresentation = buildProjectRequestRepresentation(name, startDate, expectedEndDate, endDate, description, status, budget, risk, idPerson);

		// When
		Long id = Long.parseLong(JsonPath.read(addProject(mockMvc, projectRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		// Then
		patchProject(mockMvc, id)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(id))
				.andExpect(jsonPath("$.status").value(ProjectRequestRepresentation.StatusEnum.AN_LISE_REALIZADA.getValue()));
	}

	@Test
	@Transactional
	@DisplayName("Patch /projects/{id} - Should return 200 in closed status")
	public void patchProjectsIdClosedStatusShouldReturnOk() throws Exception {
		// Given
		String namePerson = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = PersonIntegrationTest.buildPersonRequestRepresentation(namePerson, birthDate, cpf, employee);
		Long idPerson = Long.parseLong(JsonPath.read(PersonIntegrationTest.addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String name = "Teste";
		String startDate = "2000-12-31";
		String expectedEndDate = "2000-12-31";
		String description = "Teste";
		ProjectRequestRepresentation.StatusEnum status = ProjectRequestRepresentation.StatusEnum.EM_ANDAMENTO;
		BigDecimal budget = BigDecimal.ONE;
		ProjectRequestRepresentation.RiskEnum risk = ProjectRequestRepresentation.RiskEnum.BAIXO_RISCO;

		ProjectRequestRepresentation projectRequestRepresentation = buildProjectRequestRepresentation(name, startDate, expectedEndDate, null, description, status, budget, risk, idPerson);

		// When
		Long id = Long.parseLong(JsonPath.read(addProject(mockMvc, projectRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		// Then
		patchProject(mockMvc, id)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(id))
				.andExpect(jsonPath("$.status").value(ProjectRequestRepresentation.StatusEnum.ENCERRADO.getValue()))
				.andExpect(jsonPath("$.endDate").value(LocalDate.now().toString()));
	}

	@Test
	@Transactional
	@DisplayName("Patch /projects/{id} - Should return 404 on non-existent identifier")
	public void patchProjectsIdShouldReturnNotFound() throws Exception {
		// Given
		Long id = 99999999L;

		// Then
		patchProject(mockMvc, id)
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	@DisplayName("Patch /projects/{id} - Should return 400 on last status")
	public void patchProjectsIdShouldReturnBadRequestOnLastStatus() throws Exception {
		// Given
		String namePerson = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = PersonIntegrationTest.buildPersonRequestRepresentation(namePerson, birthDate, cpf, employee);
		Long idPerson = Long.parseLong(JsonPath.read(PersonIntegrationTest.addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String name = "Teste";
		String startDate = "2000-12-31";
		String expectedEndDate = "2000-12-31";
		String description = "Teste";
		ProjectRequestRepresentation.StatusEnum status = ProjectRequestRepresentation.StatusEnum.ENCERRADO;
		BigDecimal budget = BigDecimal.ONE;
		ProjectRequestRepresentation.RiskEnum risk = ProjectRequestRepresentation.RiskEnum.BAIXO_RISCO;

		ProjectRequestRepresentation projectRequestRepresentation = buildProjectRequestRepresentation(name, startDate, expectedEndDate, null, description, status, budget, risk, idPerson);

		// When
		Long id = Long.parseLong(JsonPath.read(addProject(mockMvc, projectRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		// Then
		patchProject(mockMvc, id)
				.andExpect(status().isBadRequest());
	}

	@Test
	@Transactional
	@DisplayName("Delete /projects/{id} - Should return 200 without status")
	public void deleteProjectsIdWithoutStatusShouldReturnOk() throws Exception {
		// Given
		String namePerson = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = PersonIntegrationTest.buildPersonRequestRepresentation(namePerson, birthDate, cpf, employee);
		Long idPerson = Long.parseLong(JsonPath.read(PersonIntegrationTest.addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String name = "Teste";
		String startDate = "2000-12-31";
		String expectedEndDate = "2000-12-31";
		String endDate = "2000-12-31";
		String description = "Teste";
		ProjectRequestRepresentation.StatusEnum status = null;
		BigDecimal budget = BigDecimal.ONE;
		ProjectRequestRepresentation.RiskEnum risk = ProjectRequestRepresentation.RiskEnum.BAIXO_RISCO;

		ProjectRequestRepresentation projectRequestRepresentation = buildProjectRequestRepresentation(name, startDate, expectedEndDate, endDate, description, status, budget, risk, idPerson);

		// When
		Long id = Long.parseLong(JsonPath.read(addProject(mockMvc, projectRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		// Then
		deleteProject(mockMvc, id)
				.andExpect(status().isOk());
	}

	@Test
	@Transactional
	@DisplayName("Delete /projects/{id} - Should return 200 with status")
	public void deleteProjectsIdWithStatusShouldReturnOk() throws Exception {
		// Given
		String namePerson = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = PersonIntegrationTest.buildPersonRequestRepresentation(namePerson, birthDate, cpf, employee);
		Long idPerson = Long.parseLong(JsonPath.read(PersonIntegrationTest.addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String name = "Teste";
		String startDate = "2000-12-31";
		String expectedEndDate = "2000-12-31";
		String endDate = "2000-12-31";
		String description = "Teste";
		ProjectRequestRepresentation.StatusEnum status = ProjectRequestRepresentation.StatusEnum.EM_AN_LISE;
		BigDecimal budget = BigDecimal.ONE;
		ProjectRequestRepresentation.RiskEnum risk = ProjectRequestRepresentation.RiskEnum.BAIXO_RISCO;

		ProjectRequestRepresentation projectRequestRepresentation = buildProjectRequestRepresentation(name, startDate, expectedEndDate, endDate, description, status, budget, risk, idPerson);

		// When
		Long id = Long.parseLong(JsonPath.read(addProject(mockMvc, projectRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		// Then
		deleteProject(mockMvc, id)
				.andExpect(status().isOk());
	}

	@Test
	@Transactional
	@DisplayName("Delete /projects/{id} - Should return 400 with invalid status")
	public void deleteProjectsIdWithInvalidStatusShouldReturnBadRequest() throws Exception {
		// Given
		String namePerson = "Teste";
		String birthDate = "2000-12-31";
		String cpf = "XXX.XXX.XXX-XX";
		Boolean employee = true;

		PersonRequestRepresentation personRequestRepresentation = PersonIntegrationTest.buildPersonRequestRepresentation(namePerson, birthDate, cpf, employee);
		Long idPerson = Long.parseLong(JsonPath.read(PersonIntegrationTest.addPerson(mockMvc, personRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		String name = "Teste";
		String startDate = "2000-12-31";
		String expectedEndDate = "2000-12-31";
		String endDate = "2000-12-31";
		String description = "Teste";
		ProjectRequestRepresentation.StatusEnum status = ProjectRequestRepresentation.StatusEnum.EM_ANDAMENTO;
		BigDecimal budget = BigDecimal.ONE;
		ProjectRequestRepresentation.RiskEnum risk = ProjectRequestRepresentation.RiskEnum.BAIXO_RISCO;

		ProjectRequestRepresentation projectRequestRepresentation = buildProjectRequestRepresentation(name, startDate, expectedEndDate, endDate, description, status, budget, risk, idPerson);

		// When
		Long id = Long.parseLong(JsonPath.read(addProject(mockMvc, projectRequestRepresentation)
				.andReturn()
				.getResponse()
				.getContentAsString(), "$.id").toString());

		// Then
		deleteProject(mockMvc, id)
				.andExpect(status().isBadRequest());
	}

	@Test
	@Transactional
	@DisplayName("Delete /projects/{id} - Should return 404 on non-existent identifier")
	public void deleteProjectsIdShouldReturnNotFound() throws Exception {
		// Given
		Long id = 99999999L;

		// Then
		deleteProject(mockMvc, id)
				.andExpect(status().isNotFound());
	}

	// Métodos auxiliares
	public static ResultActions addProject(MockMvc mockMvc, ProjectRequestRepresentation projectRequestRepresentation) throws Exception {
		return mockMvc.perform(MockMvcRequestBuilders
				.post("/projects")
				.contentType(MediaType.APPLICATION_JSON)
				.content(IntegrationTestUtil.convertObjectToJson(projectRequestRepresentation)));
	}

	public static ResultActions findAllProject(MockMvc mockMvc) throws Exception {
		return mockMvc.perform(MockMvcRequestBuilders
				.get("/projects")
				.contentType(MediaType.APPLICATION_JSON));
	}

	public static ResultActions findByIdProject(MockMvc mockMvc, Long id) throws Exception {
		return mockMvc.perform(MockMvcRequestBuilders
				.get("/projects/" + id)
				.contentType(MediaType.APPLICATION_JSON));
	}

	public static ResultActions putProject(MockMvc mockMvc, Long id, ProjectRequestRepresentation projectRequestRepresentation) throws Exception {
		return mockMvc.perform(MockMvcRequestBuilders
				.put("/projects/" + id)
				.contentType(MediaType.APPLICATION_JSON)
				.content(IntegrationTestUtil.convertObjectToJson(projectRequestRepresentation)));
	}

	public static ResultActions patchProject(MockMvc mockMvc, Long id) throws Exception {
		return mockMvc.perform(MockMvcRequestBuilders
				.patch("/projects/" + id)
				.contentType(MediaType.APPLICATION_JSON));
	}

	public static ResultActions deleteProject(MockMvc mockMvc, Long id) throws Exception {
		return mockMvc.perform(MockMvcRequestBuilders
				.delete("/projects/" + id)
				.contentType(MediaType.APPLICATION_JSON));
	}

	public static ProjectRequestRepresentation buildProjectRequestRepresentation(String name, String startDate, String expectedEndDate, String endDate, String description, ProjectRequestRepresentation.StatusEnum status, BigDecimal budget, ProjectRequestRepresentation.RiskEnum risk, Long managerId) {
		ProjectRequestRepresentation projectRequestRepresentation = new ProjectRequestRepresentation();
		projectRequestRepresentation.setName(name);
		projectRequestRepresentation.setStartDate(startDate);
		projectRequestRepresentation.setExpectedEndDate(expectedEndDate);
		projectRequestRepresentation.setEndDate(endDate);
		projectRequestRepresentation.setDescription(description);
		projectRequestRepresentation.setStatus(status);
		projectRequestRepresentation.setBudget(budget);
		projectRequestRepresentation.setRisk(risk);
		projectRequestRepresentation.setManagerId(managerId);

		return projectRequestRepresentation;
	}

}