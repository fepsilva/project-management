package com.fepsilva.projectmanagement.entrypoint.rest.mapper;

import br.com.fepsilva.openapi.representation.PersonRequestRepresentation;
import br.com.fepsilva.openapi.representation.PersonResponseRepresentation;
import com.fepsilva.projectmanagement.model.Person;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PersonRepresentationMapper {

	PersonResponseRepresentation toRepresentation(Person person);
	Person toModel(PersonRequestRepresentation personRequestRepresentation);

}