package com.fepsilva.projectmanagement.entrypoint.rest.controller;

import br.com.fepsilva.openapi.api.ProjectsApi;
import br.com.fepsilva.openapi.representation.ProjectRequestRepresentation;
import br.com.fepsilva.openapi.representation.ProjectResponseRepresentation;
import com.fepsilva.projectmanagement.entrypoint.rest.mapper.ProjectRepresentationMapper;
import com.fepsilva.projectmanagement.model.Project;
import com.fepsilva.projectmanagement.usecase.project.AdvanceStatusProjectUseCase;
import com.fepsilva.projectmanagement.usecase.project.CreateProjectUseCase;
import com.fepsilva.projectmanagement.usecase.project.DeleteProjectUseCase;
import com.fepsilva.projectmanagement.usecase.project.FindByIdProjectUseCase;
import com.fepsilva.projectmanagement.usecase.project.ListProjectUseCase;
import com.fepsilva.projectmanagement.usecase.project.UpdateProjectUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class ProjectController implements ProjectsApi {

	@Autowired
	private CreateProjectUseCase createProjectUseCase;

	@Autowired
	private ListProjectUseCase listProjectUseCase;

	@Autowired
	private FindByIdProjectUseCase findByIdProjectUseCase;

	@Autowired
	private UpdateProjectUseCase updateProjectUseCase;

	@Autowired
	private AdvanceStatusProjectUseCase advanceStatusProjectUseCase;

	@Autowired
	private DeleteProjectUseCase deleteProjectUseCase;

	@Autowired
	private ProjectRepresentationMapper projectRepresentationMapper;

	@Override
	public ResponseEntity<ProjectResponseRepresentation> addProject(ProjectRequestRepresentation projectRepresentation) {
		Project project = createProjectUseCase.create(projectRepresentationMapper.toModel(projectRepresentation));
		return ResponseEntity.status(HttpStatus.CREATED).body(projectRepresentationMapper.toRepresentation(project));
	}

	@Override
	public ResponseEntity<List<ProjectResponseRepresentation>> findAllProject() {
		return ResponseEntity.ok().body(Optional.ofNullable(listProjectUseCase.list())
				.orElseGet(ArrayList::new)
				.stream().map(projectRepresentationMapper::toRepresentation)
				.toList());
	}

	@Override
	public ResponseEntity<ProjectResponseRepresentation> findByIdProject(Long id) {
		Project project = findByIdProjectUseCase.find(id);
		return ResponseEntity.ok().body(projectRepresentationMapper.toRepresentation(project));
	}

	@Override
	public ResponseEntity<ProjectResponseRepresentation> updateProject(Long id, ProjectRequestRepresentation projectRepresentation) {
		Project project = updateProjectUseCase.update(id, projectRepresentationMapper.toModel(projectRepresentation));
		return ResponseEntity.ok().body(projectRepresentationMapper.toRepresentation(project));
	}

	@Override
	public ResponseEntity<ProjectResponseRepresentation> advanceStatusProject(Long id) {
		Project project = advanceStatusProjectUseCase.advance(id);
		return ResponseEntity.ok().body(projectRepresentationMapper.toRepresentation(project));
	}

	@Override
	public ResponseEntity<Void> deleteProject(Long id) {
		deleteProjectUseCase.delete(id);
		return ResponseEntity.ok().build();
	}

}