package com.fepsilva.projectmanagement.entrypoint.rest.exception;

import br.com.fepsilva.openapi.representation.ErrorResponseRepresentation;
import com.fepsilva.projectmanagement.exception.InvalidStatusDeleteException;
import com.fepsilva.projectmanagement.exception.LastStatusException;
import com.fepsilva.projectmanagement.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;
import java.util.Optional;

@ControllerAdvice
public class ControlExceptionHandler {

	private static final String FIELD_NOT_VALID = "Field %s is mandatory";

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception) {
		BindingResult bindingResult = exception.getBindingResult();
		List<String> fieldErrorDtos = bindingResult.getFieldErrors().stream()
				.map(f -> String.format(FIELD_NOT_VALID, f.getField()))
				.map(String::new)
				.toList();

		ErrorResponseRepresentation errorResponseRepresentation = new ErrorResponseRepresentation();
		errorResponseRepresentation.setMessage(fieldErrorDtos.toString());

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponseRepresentation);
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException exception) {
		String errorMessage = Optional.ofNullable(exception.getCause())
				.map(Throwable::getCause)
				.map(Throwable::getMessage)
				.orElse(null);

		ErrorResponseRepresentation errorResponseRepresentation = new ErrorResponseRepresentation();
		errorResponseRepresentation.setMessage(errorMessage);

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponseRepresentation);
	}

	@ExceptionHandler(InvalidStatusDeleteException.class)
	public ResponseEntity<Object> handleInvalidStatusDelete(InvalidStatusDeleteException exception) {
		ErrorResponseRepresentation errorResponseRepresentation = new ErrorResponseRepresentation();
		errorResponseRepresentation.setMessage(Optional.ofNullable(exception.getMessage()).orElse(exception.toString()));

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponseRepresentation);
	}

	@ExceptionHandler(LastStatusException.class)
	public ResponseEntity<Object> handleLastStatusException(LastStatusException exception) {
		ErrorResponseRepresentation errorResponseRepresentation = new ErrorResponseRepresentation();
		errorResponseRepresentation.setMessage(Optional.ofNullable(exception.getMessage()).orElse(exception.toString()));

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponseRepresentation);
	}

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<Object> handleNotFound(NotFoundException exception) {
		ErrorResponseRepresentation errorResponseRepresentation = new ErrorResponseRepresentation();
		errorResponseRepresentation.setMessage(Optional.ofNullable(exception.getMessage()).orElse(exception.toString()));

		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponseRepresentation);
	}

}