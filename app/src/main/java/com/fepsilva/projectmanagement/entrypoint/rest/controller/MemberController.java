package com.fepsilva.projectmanagement.entrypoint.rest.controller;

import br.com.fepsilva.openapi.api.MembersApi;
import br.com.fepsilva.openapi.representation.MemberRequestRepresentation;
import br.com.fepsilva.openapi.representation.MemberResponseRepresentation;
import com.fepsilva.projectmanagement.entrypoint.rest.mapper.MemberRepresentationMapper;
import com.fepsilva.projectmanagement.model.Member;
import com.fepsilva.projectmanagement.usecase.member.CreateMemberUseCase;
import com.fepsilva.projectmanagement.usecase.member.DeleteMemberUseCase;
import com.fepsilva.projectmanagement.usecase.member.FindByIdMemberUseCase;
import com.fepsilva.projectmanagement.usecase.member.ListMemberUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class MemberController implements MembersApi {

	@Autowired
	private CreateMemberUseCase createMemberUseCase;

	@Autowired
	private ListMemberUseCase listMemberUseCase;

	@Autowired
	private FindByIdMemberUseCase findByIdMemberUseCase;

	@Autowired
	private DeleteMemberUseCase deleteMemberUseCase;

	@Autowired
	private MemberRepresentationMapper memberRepresentationMapper;

	@Override
	public ResponseEntity<MemberResponseRepresentation> addMember(MemberRequestRepresentation memberRepresentation) {
		Member member = createMemberUseCase.create(memberRepresentation.getIdProject(), memberRepresentation.getNamePerson(), memberRepresentation.getRolePerson());
		return ResponseEntity.status(HttpStatus.CREATED).body(memberRepresentationMapper.toRepresentation(member));
	}

	@Override
	public ResponseEntity<List<MemberResponseRepresentation>> findAllMember() {
		return ResponseEntity.ok().body(Optional.ofNullable(listMemberUseCase.list())
				.orElseGet(ArrayList::new)
				.stream().map(memberRepresentationMapper::toRepresentation)
				.toList());
	}

	@Override
	public ResponseEntity<MemberResponseRepresentation> findByIdMember(Long id) {
		Member member = findByIdMemberUseCase.find(id);
		return ResponseEntity.ok().body(memberRepresentationMapper.toRepresentation(member));
	}

	@Override
	public ResponseEntity<Void> deleteMember(Long id) {
		deleteMemberUseCase.delete(id);
		return ResponseEntity.ok().build();
	}

}