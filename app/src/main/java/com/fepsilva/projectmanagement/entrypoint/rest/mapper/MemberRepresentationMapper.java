package com.fepsilva.projectmanagement.entrypoint.rest.mapper;

import br.com.fepsilva.openapi.representation.MemberResponseRepresentation;
import com.fepsilva.projectmanagement.model.Member;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MemberRepresentationMapper {

	MemberResponseRepresentation toRepresentation(Member member);

}