package com.fepsilva.projectmanagement.entrypoint.rest.controller;

import br.com.fepsilva.openapi.api.PersonsApi;
import br.com.fepsilva.openapi.representation.PersonRequestRepresentation;
import br.com.fepsilva.openapi.representation.PersonResponseRepresentation;
import com.fepsilva.projectmanagement.entrypoint.rest.mapper.PersonRepresentationMapper;
import com.fepsilva.projectmanagement.model.Person;
import com.fepsilva.projectmanagement.usecase.person.CreatePersonUseCase;
import com.fepsilva.projectmanagement.usecase.person.DeletePersonUseCase;
import com.fepsilva.projectmanagement.usecase.person.FindByIdPersonUseCase;
import com.fepsilva.projectmanagement.usecase.person.ListPersonUseCase;
import com.fepsilva.projectmanagement.usecase.person.UpdatePersonUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class PersonController implements PersonsApi {

	@Autowired
	private CreatePersonUseCase createPersonUseCase;

	@Autowired
	private ListPersonUseCase listPersonUseCase;

	@Autowired
	private FindByIdPersonUseCase findByIdPersonUseCase;

	@Autowired
	private UpdatePersonUseCase updatePersonUseCase;

	@Autowired
	private DeletePersonUseCase deletePersonUseCase;

	@Autowired
	private PersonRepresentationMapper personRepresentationMapper;

	@Override
	public ResponseEntity<PersonResponseRepresentation> addPerson(PersonRequestRepresentation personRepresentation) {
		Person person = createPersonUseCase.create(personRepresentationMapper.toModel(personRepresentation));
		return ResponseEntity.status(HttpStatus.CREATED).body(personRepresentationMapper.toRepresentation(person));
	}

	@Override
	public ResponseEntity<List<PersonResponseRepresentation>> findAllPerson() {
		return ResponseEntity.ok().body(Optional.ofNullable(listPersonUseCase.list())
				.orElseGet(ArrayList::new)
				.stream().map(personRepresentationMapper::toRepresentation)
				.toList());
	}

	@Override
	public ResponseEntity<PersonResponseRepresentation> findByIdPerson(Long id) {
		Person person = findByIdPersonUseCase.find(id);
		return ResponseEntity.ok().body(personRepresentationMapper.toRepresentation(person));
	}

	@Override
	public ResponseEntity<PersonResponseRepresentation> updatePerson(Long id, PersonRequestRepresentation personRepresentation) {
		Person person = updatePersonUseCase.update(id, personRepresentationMapper.toModel(personRepresentation));
		return ResponseEntity.ok().body(personRepresentationMapper.toRepresentation(person));
	}

	@Override
	public ResponseEntity<Void> deletePerson(Long id) {
		deletePersonUseCase.delete(id);
		return ResponseEntity.ok().build();
	}

}