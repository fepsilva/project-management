package com.fepsilva.projectmanagement.entrypoint.rest.mapper;

import br.com.fepsilva.openapi.representation.ProjectRequestRepresentation;
import br.com.fepsilva.openapi.representation.ProjectResponseRepresentation;
import com.fepsilva.projectmanagement.constant.RiskModelEnum;
import com.fepsilva.projectmanagement.constant.StatusModelEnum;
import com.fepsilva.projectmanagement.model.Project;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface ProjectRepresentationMapper {

	@Named("mapRiskModelEnum")
	default ProjectResponseRepresentation.RiskEnum mapRiskModelEnum(RiskModelEnum riskModelEnum) {
		if (riskModelEnum != null) {
			switch (riskModelEnum) {
				case LOW_RISK: return ProjectResponseRepresentation.RiskEnum.BAIXO_RISCO;
				case MEDIUM_RISK: return ProjectResponseRepresentation.RiskEnum.M_DIO_RISCO;
				case HIGH_RISK: return ProjectResponseRepresentation.RiskEnum.ALTO_RISCO;
			}
		}

		return null;
	}

	@Named("mapStatusModelEnum")
	default ProjectResponseRepresentation.StatusEnum mapStatusModelEnum(StatusModelEnum statusModelEnum) {
		if (statusModelEnum != null) {
			switch (statusModelEnum) {
				case UNDER_ANALYSIS: return ProjectResponseRepresentation.StatusEnum.EM_AN_LISE;
				case ANALYSIS_PERFORMED: return ProjectResponseRepresentation.StatusEnum.AN_LISE_REALIZADA;
				case APPROVED_ANALYSIS: return ProjectResponseRepresentation.StatusEnum.AN_LISE_APROVADA;
				case STARTED: return ProjectResponseRepresentation.StatusEnum.INICIADO;
				case PLANNED: return ProjectResponseRepresentation.StatusEnum.PLANEJADO;
				case IN_PROGRESS: return ProjectResponseRepresentation.StatusEnum.EM_ANDAMENTO;
				case CLOSED: return ProjectResponseRepresentation.StatusEnum.ENCERRADO;
				case CANCELED: return ProjectResponseRepresentation.StatusEnum.CANCELADO;
			}
		}

		return null;
	}

	@Named("mapRiskRequestEnum")
	default RiskModelEnum mapRiskRequestEnum(ProjectRequestRepresentation.RiskEnum riskEnum) {
		if (riskEnum != null) {
			switch (riskEnum) {
				case BAIXO_RISCO: return RiskModelEnum.LOW_RISK;
				case M_DIO_RISCO: return RiskModelEnum.MEDIUM_RISK;
				case ALTO_RISCO: return RiskModelEnum.HIGH_RISK;
			}
		}

		return null;
	}

	@Named("mapStatusRequestEnum")
	default StatusModelEnum mapStatusRequestEnum(ProjectRequestRepresentation.StatusEnum statusEnum) {
		if (statusEnum != null) {
			switch (statusEnum) {
				case EM_AN_LISE: return StatusModelEnum.UNDER_ANALYSIS;
				case AN_LISE_REALIZADA: return StatusModelEnum.ANALYSIS_PERFORMED;
				case AN_LISE_APROVADA: return StatusModelEnum.APPROVED_ANALYSIS;
				case INICIADO: return StatusModelEnum.STARTED;
				case PLANEJADO: return StatusModelEnum.PLANNED;
				case EM_ANDAMENTO: return StatusModelEnum.IN_PROGRESS;
				case ENCERRADO: return StatusModelEnum.CLOSED;
				case CANCELADO: return StatusModelEnum.CANCELED;
			}
		}

		return null;
	}

	@Mapping(target = "risk", source = "risk", qualifiedByName = "mapRiskModelEnum")
	@Mapping(target = "status", source = "status", qualifiedByName = "mapStatusModelEnum")
	ProjectResponseRepresentation toRepresentation(Project project);

	@Mapping(target = "risk", source = "risk", qualifiedByName = "mapRiskRequestEnum")
	@Mapping(target = "status", source = "status", qualifiedByName = "mapStatusRequestEnum")
	Project toModel(ProjectRequestRepresentation projectRequestRepresentation);

}