# Postman

Está é a collection para poder realizar os testes local

## Requisitos

- Ter o Postman instalado, caso não tenha utilize este <a href="https://www.postman.com/downloads/" download>link</a>

## Instalação

1. Baixe o arquivo [Project Management.postman_collection.json](Project%20Management.postman_collection.json "download")
2. Abra o Postman
3. Clique no botão **Import**

![Print](postman01.jpg)

4. Selecione a opção **File**

![Print](postman02.jpg)

5. Selecione o arquivo que foi baixado

6. Clique no botão **Import**

![Print](postman03.jpg)

7. Pronto, agora só realizar os testes

![Print](postman04.jpg)

[<< Voltar para página principal](/README.md)