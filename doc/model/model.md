# Modelagem da Aplicação

Essa documentação é para exibir o modelo do banco de dados

## Modelo

![model01.jpg](model01.jpg)

## Dicionário de Dados

<details><summary>PESSOA</summary>

- **ID:** Id da pessoa
- **NOME:** Nome da pessoa
- **DATANASCIMENTO:** Data de nascimento da pessoa
- **CPF:** CPF da pessoa
- **FUNCIONARIO:** Flag de funcionario da pessoa
</details>

<details><summary>PROJETO</summary>

- **ID:** Id do projeto
- **NOME:** Nome do projeto
- **DATA_INICIO:** Data de inicio do projeto
- **DATA_PREVISAO_FIM:** previsao do fim do projeto
- **DATA_FIM:** Data do fim do projeto
- **DESCRICAO:** Descricao do projeto
- **STATUS:** Status do projeto
- **ORCAMENTO:** Orcamento do projeto
- **RISCO:** Status do projeto
- **IDGERENTE:** Id do gerente do projeto
</details>

<details><summary>MEMBROS</summary>

- **IDPROJETO:** Id do projeto
- **IDPESSOA:** Id da pessoa
</details>


[<< Voltar para página principal](/README.md)
