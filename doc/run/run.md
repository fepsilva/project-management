# Execução do projeto

Essa documentação é para ensinar como rodar o projeto local na máquina

## Requisitos

- Ter o Git instalado, caso não tenha utilize este <a href="https://git-scm.com/downloads" download>link</a>
- Ter uma IDE instalada, para este exemplo iremos utilizar o IntelliJ IDEA Community Edition, caso não tenha pode baixar no <a href="https://www.jetbrains.com/idea/download/#section=windows">link</a>

## Passo a Passo

1. Este projeto utiliza api first, é necessário executar o mvn clean install ou o mvn clean compile

2. Realize o clone do projeto através do comando

``git clone https://gitlab.com/fepsilva/project-management.git``

![run01.jpg](run01.jpg)

3. Como pode ver ele irá realizar o download corretamente na branch **develop**

![run02.jpg](run02.jpg)

4. Abra o IntelliJ IDEA Community Edition e selecione o botão **Open**

![run03.jpg](run03.jpg)

5. Ache nas suas pastas o projeto que acabou de clonar e abra ele

![run04.jpg](run04.jpg)

6. Ache a classe de inicialização conforme demonstrado e clique no botão verde de **Play**

![run05.jpg](run05.jpg)

7. Este projeto utiliza Lombok, caso aparece uma janela pedindo para habilitar, clique em sim

![run06.jpg](run06.jpg)

8. O projeto deve subir sem nenhum problema

![run06.jpg](run07.jpg)

[<< Voltar para página principal](/README.md)