# Projeto de Gerenciamento de Projetos

Projeto para gerenciar os projetos de uma consultoria

## Requisitos Gerais

1. Para edição do projeto é necessária uma IDE (recomendada a mesma que foi utilizada no desenvolvimento)
2. Para teste das APIs é recomendado a utilização do **Postman v10.10.3** (Para mais detalhes acesse o item [Collection no Postman para testes](/doc/postman/postman.md))
3. É necessário baixar a <a href="https://www.oracle.com/java/technologies/downloads/#java17">**Java SE 17**</a>

## Documentação

1. [Requisitos do Projeto](doc/requirements/Project%20Requirements.pdf)
2. [Modelagem da Aplicação](doc/model/model.md)
3. [Execução do Projeto](/doc/run/run.md)
4. [Documentação das APIs](/doc/swagger/openapi.yaml)
5. [Collection no Postman para testes](/doc/postman/postman.md)

## Utilizados no Desenvolvimento

1. JDK: <a href="https://www.oracle.com/java/technologies/downloads/#java17">**Java SE 17**</a>
2. IDE: <a href="https://www.jetbrains.com/idea/download/#section=windows">**IntelliJ IDEA Community Edition**</a>
3. GIT: <a href="https://gitlab.com/">**GitLab**</a>