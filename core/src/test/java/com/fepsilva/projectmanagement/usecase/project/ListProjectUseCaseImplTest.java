package com.fepsilva.projectmanagement.usecase.project;

import com.fepsilva.projectmanagement.model.Project;
import com.fepsilva.projectmanagement.repository.ProjectRepository;
import com.fepsilva.projectmanagement.usecase.project.impl.ListProjectUseCaseImpl;
import com.fepsilva.projectmanagement.usecase.project.util.ProjectUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class ListProjectUseCaseImplTest {

	@Mock
	private ProjectRepository projectRepository;

	@InjectMocks
	private ListProjectUseCaseImpl listProjectUseCaseImpl;

	@Test
	@DisplayName("Method list() - Should execute successful")
	public void listSuccessful() {
		// Given
		List<Project> listProjectSaved = List.of(
				ProjectUtil.buildProject(1L, null, null, null, null, null, null, null, null, null),
				ProjectUtil.buildProject(2L, null, null, null, null, null, null, null, null, null));

		// When
		Mockito.when(this.projectRepository.list()).thenReturn(listProjectSaved);

		// Then
		List<Project> result = listProjectUseCaseImpl.list();
		Assertions.assertNotNull(result);
		Assertions.assertEquals(result.size(), 2);
	}

}