package com.fepsilva.projectmanagement.usecase.project.util;

import com.fepsilva.projectmanagement.constant.RiskModelEnum;
import com.fepsilva.projectmanagement.constant.StatusModelEnum;
import com.fepsilva.projectmanagement.model.Project;

import java.math.BigDecimal;
import java.time.LocalDate;

public class ProjectUtil {

	public static Project buildProject(Long id, String name, LocalDate startDate, LocalDate expectedEndDate, LocalDate endDate, String description, StatusModelEnum status, BigDecimal budget, RiskModelEnum risk, Long managerId) {
		return Project.builder()
				.id(id)
				.name(name)
				.startDate(startDate)
				.expectedEndDate(expectedEndDate)
				.endDate(endDate)
				.description(description)
				.status(status)
				.budget(budget)
				.risk(risk)
				.managerId(managerId)
				.build();
	}

}