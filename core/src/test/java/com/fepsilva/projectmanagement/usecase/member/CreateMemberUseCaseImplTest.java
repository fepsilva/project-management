package com.fepsilva.projectmanagement.usecase.member;

import com.fepsilva.projectmanagement.model.Member;
import com.fepsilva.projectmanagement.model.Person;
import com.fepsilva.projectmanagement.model.Project;
import com.fepsilva.projectmanagement.repository.MemberRepository;
import com.fepsilva.projectmanagement.usecase.member.impl.CreateMemberUseCaseImpl;
import com.fepsilva.projectmanagement.usecase.member.util.MemberUtil;
import com.fepsilva.projectmanagement.usecase.person.FindByNamePersonUseCase;
import com.fepsilva.projectmanagement.usecase.person.util.PersonUtil;
import com.fepsilva.projectmanagement.usecase.project.FindByIdProjectUseCase;
import com.fepsilva.projectmanagement.usecase.project.util.ProjectUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CreateMemberUseCaseImplTest {

	@Mock
	private FindByIdProjectUseCase findByIdProjectUseCase;

	@Mock
	private FindByNamePersonUseCase findByNamePersonUseCase;

	@Mock
	private MemberRepository memberRepository;

	@InjectMocks
	private CreateMemberUseCaseImpl createMemberUseCaseImpl;

	@Test
	@DisplayName("Method create() - Should execute successful")
	public void createSuccessful() {
		// Given
		Long idProject = 1L;
		Long idPerson = 1L;
		String namePerson = "Teste";

		Member member = MemberUtil.buildMember(idProject, idPerson);
		Member memberSaved = MemberUtil.buildMember(idProject, idPerson);
		Person person = PersonUtil.buildPerson(idPerson, namePerson, null, null, null);
		Project project = ProjectUtil.buildProject(idProject, null, null, null, null, null, null, null, null, idPerson);

		// When
		Mockito.when(this.findByIdProjectUseCase.find(idProject)).thenReturn(project);
		Mockito.when(this.findByNamePersonUseCase.find(namePerson)).thenReturn(person);
		Mockito.when(this.memberRepository.save(member)).thenReturn(memberSaved);

		// Then
		Member result = createMemberUseCaseImpl.create(idProject, namePerson, null);
		Assertions.assertNotNull(result.getIdProject());
	}

}