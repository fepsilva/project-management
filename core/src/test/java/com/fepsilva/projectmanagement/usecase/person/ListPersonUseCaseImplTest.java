package com.fepsilva.projectmanagement.usecase.person;

import com.fepsilva.projectmanagement.model.Person;
import com.fepsilva.projectmanagement.repository.PersonRepository;
import com.fepsilva.projectmanagement.usecase.person.impl.ListPersonUseCaseImpl;
import com.fepsilva.projectmanagement.usecase.person.util.PersonUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class ListPersonUseCaseImplTest {

	@Mock
	private PersonRepository personRepository;

	@InjectMocks
	private ListPersonUseCaseImpl listPersonUseCaseImpl;

	@Test
	@DisplayName("Method list() - Should execute successful")
	public void listSuccessful() {
		// Given
		List<Person> listPersonSaved = List.of(
				PersonUtil.buildPerson(1L, null, null, null, null),
				PersonUtil.buildPerson(2L, null, null, null, null));

		// When
		Mockito.when(this.personRepository.list()).thenReturn(listPersonSaved);

		// Then
		List<Person> result = listPersonUseCaseImpl.list();
		Assertions.assertNotNull(result);
		Assertions.assertEquals(result.size(), 2);
	}

}