package com.fepsilva.projectmanagement.usecase.member;

import com.fepsilva.projectmanagement.exception.NotFoundException;
import com.fepsilva.projectmanagement.model.Member;
import com.fepsilva.projectmanagement.repository.MemberRepository;
import com.fepsilva.projectmanagement.usecase.member.util.MemberUtil;
import com.fepsilva.projectmanagement.usecase.member.impl.FindByIdMemberUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class FindByIdMemberUseCaseImplTest {

	@Mock
	private MemberRepository memberRepository;

	@InjectMocks
	private FindByIdMemberUseCaseImpl findByIdMemberUseCaseImpl;

	@Test
	@DisplayName("Method findById() - Should execute successful")
	public void findByIdSuccessful() {
		// Given
		Long idProject = 1L;
		Long idPerson = 1L;

		Member memberSaved = MemberUtil.buildMember(idProject, idPerson);

		// When
		Mockito.when(this.memberRepository.findById(idProject)).thenReturn(Optional.of(memberSaved));

		// Then
		Member result = findByIdMemberUseCaseImpl.find(idProject);
		Assertions.assertNotNull(result.getIdProject());
	}

	@Test
	@DisplayName("Method findById() - Should throw NotFoundException")
	public void findByIdShoudThrowNotFoundException() {
		// Given
		Long idProject = 1L;

		// When
		Mockito.when(this.memberRepository.findById(idProject)).thenReturn(Optional.empty());

		// Then
		Assertions.assertThrows(NotFoundException.class, () -> findByIdMemberUseCaseImpl.find(idProject));
	}

}