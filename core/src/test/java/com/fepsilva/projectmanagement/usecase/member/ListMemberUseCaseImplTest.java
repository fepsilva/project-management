package com.fepsilva.projectmanagement.usecase.member;

import com.fepsilva.projectmanagement.model.Member;
import com.fepsilva.projectmanagement.repository.MemberRepository;
import com.fepsilva.projectmanagement.usecase.member.util.MemberUtil;
import com.fepsilva.projectmanagement.usecase.member.impl.ListMemberUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class ListMemberUseCaseImplTest {

	@Mock
	private MemberRepository memberRepository;

	@InjectMocks
	private ListMemberUseCaseImpl listMemberUseCaseImpl;

	@Test
	@DisplayName("Method list() - Should execute successful")
	public void listSuccessful() {
		// Given
		List<Member> listMemberSaved = List.of(
				MemberUtil.buildMember(1L, 1L),
				MemberUtil.buildMember(2L, 2L));

		// When
		Mockito.when(this.memberRepository.list()).thenReturn(listMemberSaved);

		// Then
		List<Member> result = listMemberUseCaseImpl.list();
		Assertions.assertNotNull(result);
		Assertions.assertEquals(result.size(), 2);
	}

}