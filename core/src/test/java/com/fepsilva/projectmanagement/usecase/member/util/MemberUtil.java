package com.fepsilva.projectmanagement.usecase.member.util;

import com.fepsilva.projectmanagement.model.Member;

public class MemberUtil {

	public static Member buildMember(Long idProject, Long idPerson) {
		return Member.builder()
				.idProject(idProject)
				.idPerson(idPerson)
				.build();
	}

}