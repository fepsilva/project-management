package com.fepsilva.projectmanagement.usecase.project;

import com.fepsilva.projectmanagement.exception.NotFoundException;
import com.fepsilva.projectmanagement.model.Project;
import com.fepsilva.projectmanagement.repository.ProjectRepository;
import com.fepsilva.projectmanagement.usecase.project.impl.FindByIdProjectUseCaseImpl;
import com.fepsilva.projectmanagement.usecase.project.util.ProjectUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class FindByIdProjectUseCaseImplTest {

	@Mock
	private ProjectRepository projectRepository;

	@InjectMocks
	private FindByIdProjectUseCaseImpl findByIdProjectUseCaseImpl;

	@Test
	@DisplayName("Method findById() - Should execute successful")
	public void findByIdSuccessful() {
		// Given
		Long id = 1L;
		String name = "Teste";

		Project projectSaved = ProjectUtil.buildProject(id, name, null, null, null, null, null, null, null, null);

		// When
		Mockito.when(this.projectRepository.findById(id)).thenReturn(Optional.of(projectSaved));

		// Then
		Project result = findByIdProjectUseCaseImpl.find(id);
		Assertions.assertNotNull(result.getId());
	}

	@Test
	@DisplayName("Method findById() - Should throw NotFoundException")
	public void findByIdShoudThrowNotFoundException() {
		// Given
		Long id = 1L;

		// When
		Mockito.when(this.projectRepository.findById(id)).thenReturn(Optional.empty());

		// Then
		Assertions.assertThrows(NotFoundException.class, () -> findByIdProjectUseCaseImpl.find(id));
	}

}