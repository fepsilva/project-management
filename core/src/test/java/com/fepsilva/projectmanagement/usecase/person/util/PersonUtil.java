package com.fepsilva.projectmanagement.usecase.person.util;

import com.fepsilva.projectmanagement.model.Person;

import java.time.LocalDate;

public class PersonUtil {

	public static Person buildPerson(Long id, String name, LocalDate birthDate, String cpf, Boolean employee) {
		return Person.builder()
				.id(id)
				.name(name)
				.birthDate(birthDate)
				.cpf(cpf)
				.employee(employee)
				.build();
	}

}