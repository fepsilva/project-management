package com.fepsilva.projectmanagement.usecase.person;

import com.fepsilva.projectmanagement.exception.NotFoundException;
import com.fepsilva.projectmanagement.model.Person;
import com.fepsilva.projectmanagement.repository.PersonRepository;
import com.fepsilva.projectmanagement.usecase.person.impl.DeletePersonUseCaseImpl;
import com.fepsilva.projectmanagement.usecase.person.util.PersonUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class DeletePersonUseCaseImplTest {

	@Mock
	private PersonRepository personRepository;

	@InjectMocks
	private DeletePersonUseCaseImpl deletePersonUseCaseImpl;

	@Test
	@DisplayName("Method delete() - Should execute successful")
	public void deleteSuccessful() {
		// Given
		Long id = 1L;
		String name = "Teste";

		Person personSaved = PersonUtil.buildPerson(id, name, null, null, null);

		// When
		Mockito.when(this.personRepository.findById(id)).thenReturn(Optional.of(personSaved));

		// Then
		Assertions.assertDoesNotThrow(() -> deletePersonUseCaseImpl.delete(id));
	}

	@Test
	@DisplayName("Method delete() - Should throw NotFoundException")
	public void deleteShoudThrowNotFoundException() {
		// Given
		Long id = 1L;

		// When
		Mockito.when(this.personRepository.findById(id)).thenReturn(Optional.empty());

		// Then
		Assertions.assertThrows(NotFoundException.class, () -> deletePersonUseCaseImpl.delete(id));
	}

}