package com.fepsilva.projectmanagement.usecase.person;

import com.fepsilva.projectmanagement.model.Person;
import com.fepsilva.projectmanagement.repository.PersonRepository;
import com.fepsilva.projectmanagement.usecase.person.impl.CreatePersonUseCaseImpl;
import com.fepsilva.projectmanagement.usecase.person.util.PersonUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CreatePersonUseCaseImplTest {

	@Mock
	private PersonRepository personRepository;

	@InjectMocks
	private CreatePersonUseCaseImpl createPersonUseCaseImpl;

	@Test
	@DisplayName("Method create() - Should execute successful")
	public void createSuccessful() {
		// Given
		Long id = 1L;
		String name = "Teste";

		Person person = PersonUtil.buildPerson(null, name, null, null, null);
		Person personSaved = PersonUtil.buildPerson(id, name, null, null, null);

		// When
		Mockito.when(this.personRepository.save(person)).thenReturn(personSaved);

		// Then
		Person result = createPersonUseCaseImpl.create(person);
		Assertions.assertNotNull(result.getId());
	}

}