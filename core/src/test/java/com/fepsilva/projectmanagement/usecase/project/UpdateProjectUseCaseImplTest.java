package com.fepsilva.projectmanagement.usecase.project;

import com.fepsilva.projectmanagement.exception.NotFoundException;
import com.fepsilva.projectmanagement.model.Project;
import com.fepsilva.projectmanagement.repository.ProjectRepository;
import com.fepsilva.projectmanagement.usecase.project.impl.UpdateProjectUseCaseImpl;
import com.fepsilva.projectmanagement.usecase.project.util.ProjectUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class UpdateProjectUseCaseImplTest {

	@Mock
	private ProjectRepository projectRepository;

	@InjectMocks
	private UpdateProjectUseCaseImpl updateProjectUseCaseImpl;

	@Test
	@DisplayName("Method update() - Should execute successful")
	public void updateSuccessful() {
		// Given
		Long id = 1L;
		String name = "Teste";

		Project project = ProjectUtil.buildProject(null, name, null, null, null, null, null, null, null, null);
		Project projectSaved = ProjectUtil.buildProject(id, name, null, null, null, null, null, null, null, null);

		// When
		Mockito.when(this.projectRepository.findById(id)).thenReturn(Optional.of(projectSaved));
		Mockito.when(this.projectRepository.save(project)).thenReturn(projectSaved);

		// Then
		Project result = updateProjectUseCaseImpl.update(id, project);
		Assertions.assertNotNull(result.getId());
	}

	@Test
	@DisplayName("Method update() - Should throw NotFoundException")
	public void updateShoudThrowNotFoundException() {
		// Given
		Long id = 1L;

		// When
		Mockito.when(this.projectRepository.findById(id)).thenReturn(Optional.empty());

		// Then
		Assertions.assertThrows(NotFoundException.class, () -> updateProjectUseCaseImpl.update(id, new Project()));
	}

}