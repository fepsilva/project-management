package com.fepsilva.projectmanagement.usecase.person;

import com.fepsilva.projectmanagement.exception.NotFoundException;
import com.fepsilva.projectmanagement.model.Person;
import com.fepsilva.projectmanagement.repository.PersonRepository;
import com.fepsilva.projectmanagement.usecase.person.impl.UpdatePersonUseCaseImpl;
import com.fepsilva.projectmanagement.usecase.person.util.PersonUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class UpdatePersonUseCaseImplTest {

	@Mock
	private PersonRepository personRepository;

	@InjectMocks
	private UpdatePersonUseCaseImpl updatePersonUseCaseImpl;

	@Test
	@DisplayName("Method update() - Should execute successful")
	public void updateSuccessful() {
		// Given
		Long id = 1L;
		String name = "Teste";

		Person person = PersonUtil.buildPerson(null, name, null, null, null);
		Person personSaved = PersonUtil.buildPerson(id, name, null, null, null);

		// When
		Mockito.when(this.personRepository.findById(id)).thenReturn(Optional.of(personSaved));
		Mockito.when(this.personRepository.save(person)).thenReturn(personSaved);

		// Then
		Person result = updatePersonUseCaseImpl.update(id, person);
		Assertions.assertNotNull(result.getId());
	}

	@Test
	@DisplayName("Method update() - Should throw NotFoundException")
	public void updateShoudThrowNotFoundException() {
		// Given
		Long id = 1L;

		// When
		Mockito.when(this.personRepository.findById(id)).thenReturn(Optional.empty());

		// Then
		Assertions.assertThrows(NotFoundException.class, () -> updatePersonUseCaseImpl.update(id, new Person()));
	}

}