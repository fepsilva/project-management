package com.fepsilva.projectmanagement.usecase.project;

import com.fepsilva.projectmanagement.constant.StatusModelEnum;
import com.fepsilva.projectmanagement.exception.LastStatusException;
import com.fepsilva.projectmanagement.exception.NotFoundException;
import com.fepsilva.projectmanagement.model.Project;
import com.fepsilva.projectmanagement.repository.ProjectRepository;
import com.fepsilva.projectmanagement.usecase.project.impl.AdvanceStatusProjectUseCaseImpl;
import com.fepsilva.projectmanagement.usecase.project.util.ProjectUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class AdvanceStatusProjectUseCaseImplTest {

	@Mock
	private ProjectRepository projectRepository;

	@InjectMocks
	private AdvanceStatusProjectUseCaseImpl advanceStatusProjectUseCaseImpl;

	@Test
	@DisplayName("Method advance() - Should execute successful without previous status")
	public void advanceSuccessfulWithoutPreviousStatus() {
		// Given
		Long id = 1L;
		String name = "Teste";
		StatusModelEnum statusNext = StatusModelEnum.UNDER_ANALYSIS;

		Project project = ProjectUtil.buildProject(null, name, null, null, null, null, null, null, null, null);
		Project projectSaved = ProjectUtil.buildProject(id, name, null, null, null, null, statusNext, null, null, null);

		// When
		Mockito.when(this.projectRepository.findById(id)).thenReturn(Optional.of(project));
		Mockito.when(this.projectRepository.save(project)).thenReturn(projectSaved);

		// Then
		Project result = advanceStatusProjectUseCaseImpl.advance(id);
		Assertions.assertNotNull(result.getId());
		Assertions.assertEquals(result.getStatus(), statusNext);
	}

	@Test
	@DisplayName("Method advance() - Should execute successful with previous status")
	public void advanceSuccessfulWithPreviousStatus() {
		// Given
		Long id = 1L;
		String name = "Teste";
		StatusModelEnum status = StatusModelEnum.UNDER_ANALYSIS;
		StatusModelEnum statusNext = status.next();

		Project project = ProjectUtil.buildProject(null, name, null, null, null, null, status, null, null, null);
		Project projectSaved = ProjectUtil.buildProject(id, name, null, null, null, null, statusNext, null, null, null);

		// When
		Mockito.when(this.projectRepository.findById(id)).thenReturn(Optional.of(project));
		Mockito.when(this.projectRepository.save(project)).thenReturn(projectSaved);

		// Then
		Project result = advanceStatusProjectUseCaseImpl.advance(id);
		Assertions.assertNotNull(result.getId());
		Assertions.assertEquals(result.getStatus(), statusNext);
	}

	@Test
	@DisplayName("Method advance() - Should execute successful with closed status")
	public void advanceSuccessfulWithClosed() {
		// Given
		Long id = 1L;
		String name = "Teste";
		LocalDate endDate = LocalDate.now();
		StatusModelEnum status = StatusModelEnum.IN_PROGRESS;
		StatusModelEnum statusNext = status.next();

		Project project = ProjectUtil.buildProject(null, name, null, null, null, null, status, null, null, null);
		Project projectSaved = ProjectUtil.buildProject(id, name, null, null, endDate, null, statusNext, null, null, null);

		// When
		Mockito.when(this.projectRepository.findById(id)).thenReturn(Optional.of(project));
		Mockito.when(this.projectRepository.save(project)).thenReturn(projectSaved);

		// Then
		Project result = advanceStatusProjectUseCaseImpl.advance(id);
		Assertions.assertNotNull(result.getId());
		Assertions.assertEquals(result.getStatus(), statusNext);
		Assertions.assertNotNull(result.getEndDate());
	}

	@Test
	@DisplayName("Method update() - Should throw NotFoundException")
	public void updateShoudThrowNotFoundException() {
		// Given
		Long id = 1L;

		// When
		Mockito.when(this.projectRepository.findById(id)).thenReturn(Optional.empty());

		// Then
		Assertions.assertThrows(NotFoundException.class, () -> advanceStatusProjectUseCaseImpl.advance(id));
	}

	@Test
	@DisplayName("Method update() - Should throw LastStatusException")
	public void updateShoudThrowLastStatusException() {
		// Given
		Long id = 1L;
		String name = "Teste";
		StatusModelEnum status = StatusModelEnum.CLOSED;

		Project project = ProjectUtil.buildProject(null, name, null, null, null, null, status, null, null, null);

		// When
		Mockito.when(this.projectRepository.findById(id)).thenReturn(Optional.of(project));

		// Then
		Assertions.assertThrows(LastStatusException.class, () -> advanceStatusProjectUseCaseImpl.advance(id));
	}

}