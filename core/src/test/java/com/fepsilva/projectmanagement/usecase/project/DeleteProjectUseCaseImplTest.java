package com.fepsilva.projectmanagement.usecase.project;

import com.fepsilva.projectmanagement.constant.StatusModelEnum;
import com.fepsilva.projectmanagement.exception.InvalidStatusDeleteException;
import com.fepsilva.projectmanagement.exception.NotFoundException;
import com.fepsilva.projectmanagement.model.Project;
import com.fepsilva.projectmanagement.repository.ProjectRepository;
import com.fepsilva.projectmanagement.usecase.project.impl.DeleteProjectUseCaseImpl;
import com.fepsilva.projectmanagement.usecase.project.util.ProjectUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class DeleteProjectUseCaseImplTest {

	@Mock
	private ProjectRepository projectRepository;

	@InjectMocks
	private DeleteProjectUseCaseImpl deleteProjectUseCaseImpl;

	@Test
	@DisplayName("Method delete() - Should execute successful without status")
	public void deleteSuccessfulWithoutStatus() {
		// Given
		Long id = 1L;
		String name = "Teste";

		Project projectSaved = ProjectUtil.buildProject(id, name, null, null, null, null, null, null, null, null);

		// When
		Mockito.when(this.projectRepository.findById(id)).thenReturn(Optional.of(projectSaved));

		// Then
		Assertions.assertDoesNotThrow(() -> deleteProjectUseCaseImpl.delete(id));
	}

	@Test
	@DisplayName("Method delete() - Should execute successful with status")
	public void deleteSuccessfulWithStatus() {
		// Given
		Long id = 1L;
		String name = "Teste";
		StatusModelEnum status = StatusModelEnum.UNDER_ANALYSIS;

		Project projectSaved = ProjectUtil.buildProject(id, name, null, null, null, null, status, null, null, null);

		// When
		Mockito.when(this.projectRepository.findById(id)).thenReturn(Optional.of(projectSaved));

		// Then
		Assertions.assertDoesNotThrow(() -> deleteProjectUseCaseImpl.delete(id));
	}

	@Test
	@DisplayName("Method delete() - Should throw NotFoundException")
	public void deleteShoudThrowNotFoundException() {
		// Given
		Long id = 1L;

		// When
		Mockito.when(this.projectRepository.findById(id)).thenReturn(Optional.empty());

		// Then
		Assertions.assertThrows(NotFoundException.class, () -> deleteProjectUseCaseImpl.delete(id));
	}

	@Test
	@DisplayName("Method delete() - Should throw InvalidStatusDeleteException")
	public void deleteShoudThrowInvalidStatusDeleteException() {
		// Given
		Long id = 1L;
		String name = "Teste";
		StatusModelEnum status = StatusModelEnum.IN_PROGRESS;

		Project projectSaved = ProjectUtil.buildProject(id, name, null, null, null, null, status, null, null, null);

		// When
		Mockito.when(this.projectRepository.findById(id)).thenReturn(Optional.of(projectSaved));

		// Then
		Assertions.assertThrows(InvalidStatusDeleteException.class, () -> deleteProjectUseCaseImpl.delete(id));
	}

}