package com.fepsilva.projectmanagement.usecase.project;

import com.fepsilva.projectmanagement.model.Person;
import com.fepsilva.projectmanagement.model.Project;
import com.fepsilva.projectmanagement.repository.ProjectRepository;
import com.fepsilva.projectmanagement.usecase.person.FindByIdPersonUseCase;
import com.fepsilva.projectmanagement.usecase.person.util.PersonUtil;
import com.fepsilva.projectmanagement.usecase.project.impl.CreateProjectUseCaseImpl;
import com.fepsilva.projectmanagement.usecase.project.util.ProjectUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

@ExtendWith(MockitoExtension.class)
public class CreateProjectUseCaseImplTest {

	@Mock
	private FindByIdPersonUseCase findByIdPersonUseCase;

	@Mock
	private ProjectRepository projectRepository;

	@InjectMocks
	private CreateProjectUseCaseImpl createProjectUseCaseImpl;

	@Test
	@DisplayName("Method create() - Should execute successful")
	public void createSuccessful() {
		// Given
		Long id = 1L;
		Long idPerson = 1L;
		String name = "Teste";

		Person person = PersonUtil.buildPerson(idPerson, name, null, null, null);
		Project project = ProjectUtil.buildProject(null, name, null, null, null, null, null, null, null, idPerson);
		Project projectSaved = ProjectUtil.buildProject(id, name, null, null, null, null, null, null, null, idPerson);

		// When
		Mockito.when(this.findByIdPersonUseCase.find(idPerson)).thenReturn(person);
		Mockito.when(this.projectRepository.save(project)).thenReturn(projectSaved);

		// Then
		Project result = createProjectUseCaseImpl.create(project);
		Assertions.assertNotNull(result.getId());
	}

}