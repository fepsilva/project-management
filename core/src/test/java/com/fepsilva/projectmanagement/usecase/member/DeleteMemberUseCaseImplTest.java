package com.fepsilva.projectmanagement.usecase.member;

import com.fepsilva.projectmanagement.exception.NotFoundException;
import com.fepsilva.projectmanagement.model.Member;
import com.fepsilva.projectmanagement.repository.MemberRepository;
import com.fepsilva.projectmanagement.usecase.member.util.MemberUtil;
import com.fepsilva.projectmanagement.usecase.member.impl.DeleteMemberUseCaseImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class DeleteMemberUseCaseImplTest {

	@Mock
	private MemberRepository memberRepository;

	@InjectMocks
	private DeleteMemberUseCaseImpl deleteMemberUseCaseImpl;

	@Test
	@DisplayName("Method delete() - Should execute successful")
	public void deleteSuccessful() {
		// Given
		Long idProject = 1L;
		Long idPerson = 1L;

		Member memberSaved = MemberUtil.buildMember(idProject, idPerson);

		// When
		Mockito.when(this.memberRepository.findById(idProject)).thenReturn(Optional.of(memberSaved));

		// Then
		Assertions.assertDoesNotThrow(() -> deleteMemberUseCaseImpl.delete(idProject));
	}

	@Test
	@DisplayName("Method delete() - Should throw NotFoundException")
	public void deleteShoudThrowNotFoundException() {
		// Given
		Long idProject = 1L;

		// When
		Mockito.when(this.memberRepository.findById(idProject)).thenReturn(Optional.empty());

		// Then
		Assertions.assertThrows(NotFoundException.class, () -> deleteMemberUseCaseImpl.delete(idProject));
	}

}