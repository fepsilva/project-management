package com.fepsilva.projectmanagement.model;

import com.fepsilva.projectmanagement.constant.RiskModelEnum;
import com.fepsilva.projectmanagement.constant.StatusModelEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Project {

	private Long id;
	private String name;
	private LocalDate startDate;
	private LocalDate expectedEndDate;
	private LocalDate endDate;
	private String description;
	private StatusModelEnum status;
	private BigDecimal budget;
	private RiskModelEnum risk;
	private Long managerId;

}