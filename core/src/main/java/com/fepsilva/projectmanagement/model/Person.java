package com.fepsilva.projectmanagement.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Person {

	private Long id;
	private String name;
	private LocalDate birthDate;
	private String cpf;
	private Boolean employee;

}