package com.fepsilva.projectmanagement.repository;

import com.fepsilva.projectmanagement.model.Person;

import java.util.List;
import java.util.Optional;

public interface PersonRepository {

	Person save(Person person);
	List<Person> list();
	Optional<Person> findById(Long id);
	Optional<Person> findByName(String name);
	void delete(Person person);

}