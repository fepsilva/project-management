package com.fepsilva.projectmanagement.repository;

import com.fepsilva.projectmanagement.model.Project;

import java.util.List;
import java.util.Optional;

public interface ProjectRepository {

	Project save(Project project);
	List<Project> list();
	Optional<Project> findById(Long id);
	void delete(Project project);

}