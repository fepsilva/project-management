package com.fepsilva.projectmanagement.repository;

import com.fepsilva.projectmanagement.model.Member;

import java.util.List;
import java.util.Optional;

public interface MemberRepository {

	Member save(Member member);
	List<Member> list();
	Optional<Member> findById(Long id);
	void delete(Member member);

}