package com.fepsilva.projectmanagement.exception;

public class LastStatusException extends RuntimeException {

	public LastStatusException(String description) {
		super(description);
	}

}