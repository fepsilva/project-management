package com.fepsilva.projectmanagement.exception;

public class InvalidStatusDeleteException extends RuntimeException {

	public InvalidStatusDeleteException(String description) {
		super(description);
	}

}