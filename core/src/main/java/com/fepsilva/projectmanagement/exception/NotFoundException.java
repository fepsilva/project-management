package com.fepsilva.projectmanagement.exception;

public class NotFoundException extends RuntimeException {

	public NotFoundException(String description) {
		super(description);
	}

}