package com.fepsilva.projectmanagement.usecase.person.impl;

import com.fepsilva.projectmanagement.exception.NotFoundException;
import com.fepsilva.projectmanagement.model.Person;
import com.fepsilva.projectmanagement.repository.PersonRepository;
import com.fepsilva.projectmanagement.usecase.person.FindByNamePersonUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FindByNamePersonUseCaseImpl implements FindByNamePersonUseCase {

	private static final String NOT_FOUND = "Person not found";

	@Autowired
	private PersonRepository personRepository;

	@Override
	public Person find(String name) {
		return personRepository.findByName(name)
				.orElseThrow(() -> new NotFoundException(NOT_FOUND));
	}

}