package com.fepsilva.projectmanagement.usecase.person.impl;

import com.fepsilva.projectmanagement.exception.NotFoundException;
import com.fepsilva.projectmanagement.model.Person;
import com.fepsilva.projectmanagement.repository.PersonRepository;
import com.fepsilva.projectmanagement.usecase.person.UpdatePersonUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdatePersonUseCaseImpl implements UpdatePersonUseCase {

	private static final String NOT_FOUND = "Person not found to update";

	@Autowired
	private PersonRepository personRepository;

	@Override
	public Person update(Long id, Person person) {
		person.setId(validateAndGetId(id));
		return personRepository.save(person);
	}

	private Long validateAndGetId(Long id) {
		return personRepository.findById(id)
				.map(Person::getId)
				.orElseThrow(() -> new NotFoundException(NOT_FOUND));
	}

}