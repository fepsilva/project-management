package com.fepsilva.projectmanagement.usecase.project.impl;

import com.fepsilva.projectmanagement.exception.NotFoundException;
import com.fepsilva.projectmanagement.model.Project;
import com.fepsilva.projectmanagement.repository.ProjectRepository;
import com.fepsilva.projectmanagement.usecase.project.FindByIdProjectUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FindByIdProjectUseCaseImpl implements FindByIdProjectUseCase {

	private static final String NOT_FOUND = "Project not found";

	@Autowired
	private ProjectRepository projectRepository;

	@Override
	public Project find(Long id) {
		return projectRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(NOT_FOUND));
	}

}