package com.fepsilva.projectmanagement.usecase.person;

import com.fepsilva.projectmanagement.model.Person;

public interface FindByIdPersonUseCase {

	Person find(Long id);

}