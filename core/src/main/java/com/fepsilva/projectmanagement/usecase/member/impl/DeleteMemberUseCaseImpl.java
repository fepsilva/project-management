package com.fepsilva.projectmanagement.usecase.member.impl;

import com.fepsilva.projectmanagement.exception.NotFoundException;
import com.fepsilva.projectmanagement.model.Member;
import com.fepsilva.projectmanagement.repository.MemberRepository;
import com.fepsilva.projectmanagement.usecase.member.DeleteMemberUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeleteMemberUseCaseImpl implements DeleteMemberUseCase {

	private static final String NOT_FOUND = "Member not found to delete";

	@Autowired
	private MemberRepository memberRepository;

	@Override
	public void delete(Long idProject) {
		Member member = memberRepository.findById(idProject)
				.orElseThrow(() -> new NotFoundException(NOT_FOUND));

		memberRepository.delete(member);
	}

}