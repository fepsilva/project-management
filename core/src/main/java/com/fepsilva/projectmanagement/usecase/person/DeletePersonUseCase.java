package com.fepsilva.projectmanagement.usecase.person;

public interface DeletePersonUseCase {

	void delete(Long id);

}