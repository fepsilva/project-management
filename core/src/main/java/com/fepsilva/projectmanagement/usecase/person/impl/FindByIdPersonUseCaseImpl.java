package com.fepsilva.projectmanagement.usecase.person.impl;

import com.fepsilva.projectmanagement.exception.NotFoundException;
import com.fepsilva.projectmanagement.model.Person;
import com.fepsilva.projectmanagement.repository.PersonRepository;
import com.fepsilva.projectmanagement.usecase.person.FindByIdPersonUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FindByIdPersonUseCaseImpl implements FindByIdPersonUseCase {

	private static final String NOT_FOUND = "Person not found";

	@Autowired
	private PersonRepository personRepository;

	@Override
	public Person find(Long id) {
		return personRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(NOT_FOUND));
	}

}