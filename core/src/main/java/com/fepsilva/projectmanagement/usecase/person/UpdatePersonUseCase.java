package com.fepsilva.projectmanagement.usecase.person;

import com.fepsilva.projectmanagement.model.Person;

public interface UpdatePersonUseCase {

	Person update(Long id, Person person);

}