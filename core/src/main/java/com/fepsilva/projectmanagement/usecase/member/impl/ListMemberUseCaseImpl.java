package com.fepsilva.projectmanagement.usecase.member.impl;

import com.fepsilva.projectmanagement.model.Member;
import com.fepsilva.projectmanagement.repository.MemberRepository;
import com.fepsilva.projectmanagement.usecase.member.ListMemberUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListMemberUseCaseImpl implements ListMemberUseCase {

	@Autowired
	private MemberRepository memberRepository;

	@Override
	public List<Member> list() {
		return memberRepository.list();
	}

}