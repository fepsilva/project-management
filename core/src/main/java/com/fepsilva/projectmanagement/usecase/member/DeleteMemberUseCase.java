package com.fepsilva.projectmanagement.usecase.member;

public interface DeleteMemberUseCase {

	void delete(Long idProject);

}