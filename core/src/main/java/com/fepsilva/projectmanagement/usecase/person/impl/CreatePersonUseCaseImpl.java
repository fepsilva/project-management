package com.fepsilva.projectmanagement.usecase.person.impl;

import com.fepsilva.projectmanagement.model.Person;
import com.fepsilva.projectmanagement.repository.PersonRepository;
import com.fepsilva.projectmanagement.usecase.person.CreatePersonUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreatePersonUseCaseImpl implements CreatePersonUseCase {

	@Autowired
	private PersonRepository personRepository;

	@Override
	public Person create(Person person) {
		return personRepository.save(person);
	}

}