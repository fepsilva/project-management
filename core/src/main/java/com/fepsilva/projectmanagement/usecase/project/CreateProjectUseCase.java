package com.fepsilva.projectmanagement.usecase.project;

import com.fepsilva.projectmanagement.model.Project;

public interface CreateProjectUseCase {

	Project create(Project project);

}