package com.fepsilva.projectmanagement.usecase.member;

import com.fepsilva.projectmanagement.model.Member;

public interface CreateMemberUseCase {

	Member create(Long idProject, String name, String rolePerson);

}