package com.fepsilva.projectmanagement.usecase.person;

import com.fepsilva.projectmanagement.model.Person;

public interface CreatePersonUseCase {

	Person create(Person person);

}