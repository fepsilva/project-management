package com.fepsilva.projectmanagement.usecase.project;

import com.fepsilva.projectmanagement.model.Project;

public interface FindByIdProjectUseCase {

	Project find(Long id);

}