package com.fepsilva.projectmanagement.usecase.member.impl;

import com.fepsilva.projectmanagement.model.Member;
import com.fepsilva.projectmanagement.repository.MemberRepository;
import com.fepsilva.projectmanagement.usecase.member.CreateMemberUseCase;
import com.fepsilva.projectmanagement.usecase.person.FindByNamePersonUseCase;
import com.fepsilva.projectmanagement.usecase.project.FindByIdProjectUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreateMemberUseCaseImpl implements CreateMemberUseCase {

	@Autowired
	private FindByIdProjectUseCase findByIdProjectUseCase;

	@Autowired
	private FindByNamePersonUseCase findByNamePersonUseCase;

	@Autowired
	private MemberRepository memberRepository;

	@Override
	public Member create(Long idProject, String name, String rolePerson) {
		Member member = Member.builder()
				.idProject(getIdProject(idProject))
				.idPerson(searchPersonByName(name))
				.build();

		return memberRepository.save(member);
	}

	private Long getIdProject(Long idProject) {
		return findByIdProjectUseCase.find(idProject).getId();
	}

	private Long searchPersonByName(String name) {
		return findByNamePersonUseCase.find(name).getId();
	}

}