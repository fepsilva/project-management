package com.fepsilva.projectmanagement.usecase.project.impl;

import com.fepsilva.projectmanagement.model.Project;
import com.fepsilva.projectmanagement.repository.ProjectRepository;
import com.fepsilva.projectmanagement.usecase.project.ListProjectUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListProjectUseCaseImpl implements ListProjectUseCase {

	@Autowired
	private ProjectRepository projectRepository;

	@Override
	public List<Project> list() {
		return projectRepository.list();
	}

}