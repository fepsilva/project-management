package com.fepsilva.projectmanagement.usecase.person;

import com.fepsilva.projectmanagement.model.Person;

public interface FindByNamePersonUseCase {

	Person find(String name);

}