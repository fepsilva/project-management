package com.fepsilva.projectmanagement.usecase.project.impl;

import com.fepsilva.projectmanagement.model.Project;
import com.fepsilva.projectmanagement.repository.ProjectRepository;
import com.fepsilva.projectmanagement.usecase.person.FindByIdPersonUseCase;
import com.fepsilva.projectmanagement.usecase.project.CreateProjectUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreateProjectUseCaseImpl implements CreateProjectUseCase {

	@Autowired
	private FindByIdPersonUseCase findByIdPersonUseCase;

	@Autowired
	private ProjectRepository projectRepository;

	@Override
	public Project create(Project project) {
		project.setManagerId(getIdPerson(project.getManagerId()));
		return projectRepository.save(project);
	}

	private Long getIdPerson(Long idPerson) {
		return findByIdPersonUseCase.find(idPerson).getId();
	}

}