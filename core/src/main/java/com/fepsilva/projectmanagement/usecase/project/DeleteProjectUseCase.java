package com.fepsilva.projectmanagement.usecase.project;

public interface DeleteProjectUseCase {

	void delete(Long id);

}