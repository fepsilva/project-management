package com.fepsilva.projectmanagement.usecase.project.impl;

import com.fepsilva.projectmanagement.exception.NotFoundException;
import com.fepsilva.projectmanagement.model.Project;
import com.fepsilva.projectmanagement.repository.ProjectRepository;
import com.fepsilva.projectmanagement.usecase.project.UpdateProjectUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdateProjectUseCaseImpl implements UpdateProjectUseCase {

	private static final String NOT_FOUND = "Project not found to update";

	@Autowired
	private ProjectRepository projectRepository;

	@Override
	public Project update(Long id, Project project) {
		project.setId(validateAndGetId(id));
		return projectRepository.save(project);
	}

	private Long validateAndGetId(Long id) {
		return projectRepository.findById(id)
				.map(Project::getId)
				.orElseThrow(() -> new NotFoundException(NOT_FOUND));
	}

}