package com.fepsilva.projectmanagement.usecase.member.impl;

import com.fepsilva.projectmanagement.exception.NotFoundException;
import com.fepsilva.projectmanagement.model.Member;
import com.fepsilva.projectmanagement.repository.MemberRepository;
import com.fepsilva.projectmanagement.usecase.member.FindByIdMemberUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FindByIdMemberUseCaseImpl implements FindByIdMemberUseCase {

	private static final String NOT_FOUND = "Member not found";

	@Autowired
	private MemberRepository memberRepository;

	@Override
	public Member find(Long idProject) {
		return memberRepository.findById(idProject)
				.orElseThrow(() -> new NotFoundException(NOT_FOUND));
	}

}