package com.fepsilva.projectmanagement.usecase.member;

import com.fepsilva.projectmanagement.model.Member;

import java.util.List;

public interface ListMemberUseCase {

	List<Member> list();

}