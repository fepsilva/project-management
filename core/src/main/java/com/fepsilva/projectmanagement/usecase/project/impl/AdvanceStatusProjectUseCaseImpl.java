package com.fepsilva.projectmanagement.usecase.project.impl;

import com.fepsilva.projectmanagement.constant.StatusModelEnum;
import com.fepsilva.projectmanagement.exception.LastStatusException;
import com.fepsilva.projectmanagement.exception.NotFoundException;
import com.fepsilva.projectmanagement.model.Project;
import com.fepsilva.projectmanagement.repository.ProjectRepository;
import com.fepsilva.projectmanagement.usecase.project.AdvanceStatusProjectUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class AdvanceStatusProjectUseCaseImpl implements AdvanceStatusProjectUseCase {

	private static final String NOT_FOUND = "Project not found for advance status";
	private static final String LAST_STATUS = "Project is in the last available status";

	@Autowired
	private ProjectRepository projectRepository;

	@Override
	public Project advance(Long id) {
		Project project = projectRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(NOT_FOUND));

		if (project.getStatus() == null) {
			project.setStatus(StatusModelEnum.UNDER_ANALYSIS);
		} else if (project.getStatus().next() != null) {
			project.setStatus(project.getStatus().next());

			if (project.getStatus() == StatusModelEnum.CLOSED) {
				project.setEndDate(LocalDate.now());
			}
		} else {
			throw new LastStatusException(LAST_STATUS);
		}

		return projectRepository.save(project);
	}

}