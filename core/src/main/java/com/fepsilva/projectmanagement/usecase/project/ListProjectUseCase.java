package com.fepsilva.projectmanagement.usecase.project;

import com.fepsilva.projectmanagement.model.Project;

import java.util.List;

public interface ListProjectUseCase {

	List<Project> list();

}