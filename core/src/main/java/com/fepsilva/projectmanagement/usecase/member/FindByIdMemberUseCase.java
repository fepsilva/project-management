package com.fepsilva.projectmanagement.usecase.member;

import com.fepsilva.projectmanagement.model.Member;

public interface FindByIdMemberUseCase {

	Member find(Long idProject);

}