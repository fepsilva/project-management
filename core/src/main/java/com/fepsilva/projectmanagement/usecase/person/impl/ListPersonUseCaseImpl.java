package com.fepsilva.projectmanagement.usecase.person.impl;

import com.fepsilva.projectmanagement.model.Person;
import com.fepsilva.projectmanagement.repository.PersonRepository;
import com.fepsilva.projectmanagement.usecase.person.ListPersonUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListPersonUseCaseImpl implements ListPersonUseCase {

	@Autowired
	private PersonRepository personRepository;

	@Override
	public List<Person> list() {
		return personRepository.list();
	}

}