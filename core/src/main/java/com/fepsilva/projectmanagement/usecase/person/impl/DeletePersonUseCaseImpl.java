package com.fepsilva.projectmanagement.usecase.person.impl;

import com.fepsilva.projectmanagement.exception.NotFoundException;
import com.fepsilva.projectmanagement.model.Person;
import com.fepsilva.projectmanagement.repository.PersonRepository;
import com.fepsilva.projectmanagement.usecase.person.DeletePersonUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeletePersonUseCaseImpl implements DeletePersonUseCase {

	private static final String NOT_FOUND = "Person not found to delete";

	@Autowired
	private PersonRepository personRepository;

	@Override
	public void delete(Long id) {
		Person person = personRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(NOT_FOUND));

		personRepository.delete(person);
	}

}