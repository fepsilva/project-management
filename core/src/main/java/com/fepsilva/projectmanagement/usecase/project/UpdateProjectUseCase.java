package com.fepsilva.projectmanagement.usecase.project;

import com.fepsilva.projectmanagement.model.Project;

public interface UpdateProjectUseCase {

	Project update(Long id, Project project);

}