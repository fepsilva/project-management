package com.fepsilva.projectmanagement.usecase.project.impl;

import com.fepsilva.projectmanagement.constant.StatusModelEnum;
import com.fepsilva.projectmanagement.exception.InvalidStatusDeleteException;
import com.fepsilva.projectmanagement.exception.NotFoundException;
import com.fepsilva.projectmanagement.model.Project;
import com.fepsilva.projectmanagement.repository.ProjectRepository;
import com.fepsilva.projectmanagement.usecase.project.DeleteProjectUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeleteProjectUseCaseImpl implements DeleteProjectUseCase {

	private static final String NOT_FOUND = "Project not found to delete";
	private static final String INVALID_STATUS = "cannot delete a project in status %s";
	private static final List<StatusModelEnum> STATUS_THAT_CANT_DELETE = List.of(
			StatusModelEnum.STARTED,
			StatusModelEnum.IN_PROGRESS,
			StatusModelEnum.CLOSED);

	@Autowired
	private ProjectRepository projectRepository;

	@Override
	public void delete(Long id) {
		Project project = projectRepository.findById(id)
				.orElseThrow(() -> new NotFoundException(NOT_FOUND));

		validateDelete(project);
		projectRepository.delete(project);
	}

	private void validateDelete(Project project) {
		if (project.getStatus() != null && STATUS_THAT_CANT_DELETE.contains(project.getStatus())) {
			throw new InvalidStatusDeleteException(String.format(INVALID_STATUS, project.getStatus().getValue()));
		}
	}

}