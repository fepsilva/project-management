package com.fepsilva.projectmanagement.usecase.person;

import com.fepsilva.projectmanagement.model.Person;

import java.util.List;

public interface ListPersonUseCase {

	List<Person> list();

}