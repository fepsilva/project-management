package com.fepsilva.projectmanagement.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusModelEnum {

	UNDER_ANALYSIS("Em Análise"),
	ANALYSIS_PERFORMED("Análise Realizada"),
	APPROVED_ANALYSIS("Análise Aprovada"),
	STARTED("Iniciado"),
	PLANNED("Planejado"),
	IN_PROGRESS("Em Andamento"),
	CLOSED("Encerrado"),
	CANCELED("Cancelado");

	private String value;

	public StatusModelEnum next() {
		for (int i = 0; i < StatusModelEnum.values().length; i ++) {
			if ((this == StatusModelEnum.values()[i]) && ((i + 1) < StatusModelEnum.values().length) && (StatusModelEnum.values()[i + 1] != CANCELED)) {
				return StatusModelEnum.values()[i + 1];
			}
		}

		return null;
	}

}